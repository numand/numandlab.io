---
layout: post
title: "OSSEC"
subtitle: "Önsöz, Tanıtım, Temel Kavramlar ve Yapı"
summary: "OSSEC, sistemde oluşturulan kayıt dosyalarını (log) analiz eden; sistemde bulunan dosyaların bütünlüğünü denetleyen; kullanıcı tarafından oluşturulan güvenlik kurallarını izleyen ve uygulayan; rootkit'leri tespit eden gerçek zamanlı ve saldırılara anında cevap veren (active respond) açık kaynak kodlu bir konuk temelli izinsiz giriş tespit etme sistemidir. "
part: "Bölüm 1"
date: 2014-11-20
categories: 
- linux
- hids
---

> 2014 yılında [Ubuntu Türkiye]'nin [SUDO] dergisinin 62. sayısı için kaleme aldığım OSSEC yazısını, 62. sayı maalesef bu zaman kadar yayımlanamadığı için yazıyı burada bölümler halinde yayımlıyorum. İlerde yazıyı güncelleyebilirim.

> 1. Önsöz, Tanıtım, Temel Kavramlar ve Kurulum
> 2. <a href="{% post_url 2014-11-27-ossec-sunucu-ve-araci-kurulumu %}">Sunucu ve Aracı Kurulumu</a>
> 3. <a href="{% post_url 2014-12-04-ossec-dizin-yapisi-komutlar-ve-sunucu-ile-araci-iletisiminin-saglanmasi %}">Dizin Yapısı, Komutlar ve Sunucu ile Aracı İletişimini Sağlama</a>
> 4. <a href="{% post_url 2014-12-11-ossec-yapilandirma %}">OSSEC'in Yapılandırılması: Yeni Kayıt Dosyası Ekleme ve Çözücü ve Kural Yazma</a>
> 5. <a href="{% post_url 2014-12-18-ossec-kullanici-arayuzu %}">Kullanıcı Arayüzü Kurulumu ve Tanıtımı</a>
> 6. <a href="{% post_url 2014-12-25-ossec-analogi %}">AnaLogi Kurulumu ve Tanıtımı ve Sonsöz</a>

## Önsöz

<span class="run-in">GNU/Linux dünyasına</span> adım atanların merak ettikleri konulardan biri de işletim sisteminin güvenliği olmaktadır. Windows kullanımı sırasında karşılaştıkları birçok zararlı yazılım ve açıkların ve bu zararlı yazılım ve açıklara önlem olarak anti-virüs uygulamaları yüklemenin kullanıcılarda bıraktığı etki, *“Linux’ta anti-virüs uygulaması var mı?”*, *“Linux'a virüs bulaşır mı?”* gibi soruların sorulmasına yol açmakta. Bu sorulara, genellikle GNU/Linux dağıtımlarının Windows işletim sisteminden daha güvenli olduğunu; dağıtımlara virüs bulaşmasının pek mümkün olmadığını ve kullanıcıların uygulaması gereken bir kaç güvenlik önlemini içeren cümlelerle yanıt vermekteyiz. Tatmin olmayan kullanıcılara GNU/Linux dağıtımları için yazılmış anti-virüs uygulamalarını işaret etmekteyiz. Her ne kadar söylenenler yanlış olmasa da sevdiğimiz dağıtımlarımız güvenlik açıklarından tamamen arınmış değil. Özellikle sunucular üzerinde koşan dağıtımlar, herhangi bir ev kullanıcısına nazaran güvenlik açıklarına ve bu açıklardan faydalanılarak yapılan saldırılara daha fazla maruz kalmakta. “Poodle” ve “Shell Shock” açıkları bu gerçeği bir kez daha önümüze getirdi. Lâkin karamsar olmaya gerek yok. Yakalanan açıklar kısa sürede kapatılmakta ve gerekli güncellemeler kullanıcılara ulaştırılmakta. Ayrıca dağıtımlarımızı açıklara ve saldırılara karşı korumamızda yardımcı olan birçok uygulama ve yöntem bulunmakta. Bu uygulama ve yöntemleri tanıtmayı amaçladığım bir yazı dizisi yazmaya karar verdim. Yazı dizisinde, sunucu güvenliğine biraz daha fazla ihtimam gösterilmesi gerektiğini düşündüğümden, önce sunucuları ilgilendiren uygulama ve yöntemleri konu edinmeyi kararlaştırdım. Sunucular için sunulacak uygulama ve yöntemler elbette ki masaüstü kullanıcıları için de uygulanabilir fakat bu uygulamaların hedef kitlesi sunucular olduğundan böyle bir ayrıma gitmek durumunda kaldım. Yazı dizisi sonunda her birimizin daha güvenli bir sisteme sahip olması ve sistem güvenliğine dair az da olsa bilgimizin artmasını umuyorum.

## Tanıtım ve Temel Kavramlar

İlk tanıtacağım uygulama: [OSSEC]. OSSEC, sistemde oluşturulan kayıt dosyalarını (log) analiz eden; sistemde bulunan dosyaların bütünlüğünü denetleyen; kullanıcı tarafından oluşturulan güvenlik kurallarını izleyen ve uygulayan; rootkit'leri tespit eden gerçek zamanlı ve saldırılara anında cevap veren (active respond) açık kaynak kodlu bir konuk temelli izinsiz giriş tespit etme sistemidir ([HIDS]). Bu özellikler OSSEC'i, kayıt dosyası analizi, güvenlik bilgileri ve vakaları yönetimi ([SIEM]) ve HIDS'i birleştiren bir platform yapmaktadır. HIDS'in ne olduğunu kısaca öğrenerek OSSEC'in ne yaptığını daha iyi anlayalım. HIDS'ler, sisteme yapılan başarılı ya da başarısız izinsiz giriş denemelerini; "denials of service"; kötü amaçlı yazılım belirtileri gibi ağ trafiğine ya da konuk makineye yapılan saldırıları izleyen ve bunları raporlayan yazılım ya da donanımlara verilen addır. HIDS'ler genellikle ağ tabanlı ve konuk tabanlı olarak sınıflandırılmaktalar. Ağ temelliler, ağı gözlemleyerek ağa izinsiz bir sızmanın olup olmadığını tespit etmeye çalışırlarken; konuk temelliler ise kuruldukları makineye yapılan saldırıları tespit eden yazılımlardır. Kayıt dosyalarını analiz etme özelliği, OSSEC'in kayıt tutan hemen hemen her yazılımı inceleyecek şekilde yapılandırılmasına olanak sağlamakta. SSH ve SSL trafiğini de izleyen OSSEC, görevini yapmasını sağlayan su, sudo, Samba, vsftpd, Postfix, Apache, Nginx, Mysql, PHP ve WordPress gibi birçok yazılıma yönelik ön tanımlı kurallar barındırmakta.

## Yapı

OSSEC, sunduğu bu özellikleri iki temel bileşeni sayesinde sağlamakta: yönetici/sunucu (bundan sonra sunucu olarak kullanılacaktır) ve aracı. 

`Sunucu`, OSSEC platformunun merkezinde bulunan bileşendir. Dosya bütünlüğü denetimine, kayıt dosyalarına, kullanıcı tarafından izlenmesi istenen olaylara ve kullanıcı hesapları ve yetki denetimine ait veri tabanlarını tutmaktadır. Bütün kurallar ve ayarlar sunucu üzerinden yapılmaktadır. Bu da, tek bir sunucu üzerinden birçok aracının istenen şekilde ayarlanmasını sağlamaktadır.

`Aracı` ise, kullanıcının takip edilmesini istediği sistem ya da sistemlere kurulan ve kurulduğu sistem hakkında bilgi toplayıp, topladığı bilgiyi sunucuya yorumlaması ve kaydetmesi için gönderen programlar bütünüdür. Bazı bilgilerin gerçek zamanlı, bazılarının ise belirli sürelerde toplanması için yapılandırılabilir.

![ossec_catisi][ossec_catisi]

Sunucu ile aracı arasındaki iletişim [UDP] 1514 portu üzerinden [Blowfish] ile şifrelenmiş ve önceden sunucu ile aracı arasında paylaşılan anahtarlar aracılığıyla kurulmaktadır.

>Not: Çizim için kullanılan simgeler Iris simge temasından alınmıştır. Temaya [bu] adresten ulaşılabilir.

[Ubuntu Türkiye]: https://ubuntu-tr.net/
[SUDO]: https://sudo.ubuntu-tr.net/
*[HIDS]: Host Based Intrusion Detection System
*[SIEM]: Security Information and Event Management
[OSSEC]: https://ossec.github.io/
[HIDS]: https://en.wikipedia.org/wiki/Host-based_intrusion_detection_system
[SIEM]: https://en.wikipedia.org/wiki/Security_information_and_event_management
[ossec_catisi]: /assets/images/ossecin_calisma_yapisi.png
[UDP]: https://en.wikipedia.org/wiki/User_Datagram_Protocol
[Blowfish]: https://en.wikipedia.org/wiki/Blowfish_(cipher)
[bu]: https://download.gnome.org/teams/art.gnome.org/themes/icon/ICON-Iris.tar.bz2.mirrorlist