---
layout: post
title: "OSSEC"
subtitle: "Sunucu ve Aracı Kurulumu"
summary: "OSSEC'in temel bileşenleri olan sunucu ve aracının Ubuntu 14.10 üzerine kurulumu. "
part: "Bölüm 2"
date: 2014-11-27
categories: 
- linux
- hids
---

<span class="run-in">OSSEC'in *sunucu*, *aracı*, *yerel*</span> ve 2.7. sürümüyle eklenen *hibrid* olmak üzere 4 farklı kurulum türü bulunmakta. **Sunucu** kurulumu, yukarıda bahsettiğim OSSEC sunucusunu kurmaktadır. **Aracı** kurulumu ile yine yukarıda bahsettiğim aracının kurulumu gerçekleştirilmekte. Kullanıcının takip etmek istediği birden fazla makine varsa, bu makinelere gerekli bilgileri toplayacak aracı kurulmakta ve aracıların makinelerden topladığı bilgileri değerlendirecek ve yönetecek ayrı bir sunucu kurulmaktadır. Takip edilecek ve yönetilecek olan sistem aynı ise sunucu ve aracı beraber kurulabilir fakat OSSEC böyle sistemler için yerel kurulumu önermekte. **Yerel kurulumun** sunucu ve aracının beraber kurulduğu sistemlerden farkı, kurulum yapılan sistem dışındaki başka bir sistemin takip edilememesidir. **Hibrid kurulum** ise sunucu ve aracının beraber kurulduğu türdür. Hibrid kurulum yeni eklenen bir özellik olduğu için şu an hakkında yeteri kadar bilgi bulunmamaktadır. Bu bilgileri dikkate alarak kuruluma geçin.

OSSEC paketlerini [bu][1] adresten indirebilirsiniz. Her zaman için en güncel kararlı sürümü kullanmanızı öneririm. Bu makalenin yazıldığı tarihteki güncel Ossec sürümü 2.8.1.'dir. Bu durumda indireceğiniz paket “Server/Agent 2.8.1 – Linux/BSD” olarak adlandırılan paket olacaktır. Kuracağınız paketleri yerelinize aşağıdaki komutlarla çekin. Ben kurulum için sisteme dâhil olmayan paketlerin âdet olarak kurulduğu /opt dizinini seçtim.

{% highlight bash %}
cd /opt
sudo wget -U ossec http://www.ossec.net/files/ossec-hids-2.8.1.tar.gz
sudo wget -U ossec http://www.ossec.net/files/ossec-hids-2.8.1-checksum.txt
{% endhighlight %}

İndirdiğiniz paketin **[md5] ve [SHA256] bütünlük denetimlerini** yapın.

{% highlight bash %}
sudo cat ossec-hids-2.8.1-checksum.txt
sudo md5sum ossec-hids-2.8.1.tar.gz
sudo sha1sum ossec-hids-2.8.1.tar.gz
{% endhighlight %}

Değerler doğru ise paketi aynı dizine açın.

{% highlight bash %}
sudo tar -xf ossec-hids-2.8.1.tar.gz
{% endhighlight %}

Kuruluma geçmeden önce dosya içindeki önemli belgeleri okumanızı öneririm.

{% highlight bash %}
cd ossec-hids-2.8.1
less README
less INSTALL
{% endhighlight %}

Dosyaları okumanız bittiyse kuruluma geçebilirsiniz. OSSEC kurulmak için root kullanıcısına ihtiyaç duyuyor. Bu yüzden kurulum betiğini yetkili kullanıcı haklarıyla çalıştırın.

{% highlight bash %}
sudo ./install
{% endhighlight %}

Betiği çalıştırdığınızda OSSEC size kurulumun **hangi dilde** yapılacağını soracak.

{% highlight bash %}
 ** Türkçe kurulum için seçin [tr].
  (en/br/cn/de/el/es/fr/hu/it/jp/nl/pl/ru/sr/tr) [en]: tr
{% endhighlight %}

Türkçe'nin kısaltması olan "TR" yazıp devam ettiğinizde aşağıdaki çıktıyla karşılaşacaksınız.

{% highlight bash %}
OSSEC HIDS v2.8 Kurulum Betiği - http://www.ossec.net
 OSSEC HIDS kurulum sürecini başlatmak üzeresiniz.
 Sisteminizde önceden kurulmuş bir C derleyicisi bulunmalıdır.
 Her türlü soru, öneri ve yorumlarınız için lütfen dcid@ossec.net
 (veya daniel.cid@gmail.com) adresine e-posta gönderiniz.
  - Sistem: Linux linux 3.16.0-24-generic
  - Kullanıcı: root
  - Bilgisayar: linux
1- Ne tür kurulum yapmak istiyorsunuz (sunucu,aracı,yerel veya yardım)? yardım
{% endhighlight %}

Daha önce bahsettiğim **kurulum türlerinden hangisini** yüklemek istediğiniz sorulmakta. “yardım” yazdığınızda size kurulum türlerini tanıtan aşağıdaki çıktı üretilmekte.

{% highlight bash %}
- Üç adet kurulum seçeneği bulunmaktadır: sunucu, aracı veya yerel.
    - Eğer sunucu seçerseniz, bütün günlük kayıtlarını inceleyebilir
      e-posta ile bilgilendirilebilir, yanıt üretebilirsiniz.
      Ayrıca uzak syslog bilgisayarlarından ve 'aracı' olarak
      çalışan sistemlerden günlük kayıtlarını alabilirsiniz
      (aradaki trafik şifrelenecektir).
    - Eğer 'aracı' (istemci) seçerseniz, syslog, snort, apache vb.
      tarafından üretilen yerel dosyaları okuyabilir ve incelenmek üzere
      şifreli bir şekilde sunucuya gönderebilirsiniz.
    - Eğer 'yerel' seçerseniz, aracılardan veya harici syslog
      aygıtlarından gelen mesajları almak dışında sunucunun
      yapabildiği herşeyi yapabilirsiniz.
  - Eğer günlük kaydı inceleme sunucusu oluşturuyorsanız 'sunucu' kullanın.
  - Eğer başka bir günlük kaydı sunucunuz varsa ve incelenmesi için
    günlük kayıtlarınızı bu sunucuya yönlendirmek istiyorsanız
    'aracı' kullanın. (web sunucular, veritabanı sunucular vb. için
    idealdir)
- Eğer inceleme yapacağınız tek sisteminiz varsa 'yerel' kullanın.
{% endhighlight %}

Bu anlatımda tek bir sisteme kurulum yapıldığı için yerel seçilmiştir. Bundan sonra OSSEC'in **nereye kurulacağını** soran bir çıktıyla karşılaşacaksınız.

{% highlight bash %}
2- Kurulum ortamı hazırlanıyor.
 - OSSEC HIDS kurulacak yeri seçin [/var/ossec]:
 - Kurulum buraya yapılacak:  /var/ossec .
{% endhighlight %}

Ön tanımlı kurulum dizinine dokunmadan devam edin.

Bir sonraki ekranda OSSEC'in uyarılar karşısında sizi **e-posta hesabınız aracılığıyla bilgilendirmesini** isteyip istemediğinizi soran bir çıktıyla karşılaşacaksınız.

{% highlight bash %}
3- yapılandırılıyor: OSSEC HIDS.
  3.1- E-posta ile bilgilendirilmek ister misiniz? (e/h) [e]: e
Ossec'in sizi ağ ya da konak makinada meydana gelen uyarılardan haberdar edebilmesi için e-postanız gerek. Bu yüzden e deyip devam edin.
- E-posta adresiniz nedir? if@ubuntu-tr.net
- SMTP sunucunuz olarak bunu bulduk: gmail-smtp-in.l.google.com.
   - Kullanmak ister misiniz? (e/h) [e]: e
--- SMTP sunucu kullanılıyor:  ASPMX.L.GOOGLE.COM.
 --- SMTP sunucu kullanılıyor:  gmail-smtp-in.l.google.com.
{% endhighlight %}

Bir sonraki adımda OSSEC'in **dosya bütünlük denetimi** yapmasını isteyip istemediğiniz sorulmakta.

{% highlight bash %}
3.2- Güvenilirlik/bütünlük kontrol programının çalıştırılmasını ister misiniz?
{% endhighlight %}

Bütünlük denetimi süreci, sistemdeki dosyaların bütünlüğünü gözlemlemek ve raporlamakla sorumludur. Bu yüzden “e” yazıp devam edin. Çıktı olarak aşağıdaki ifadeyi alacaksınız.

{% highlight bash %}
- syscheck çalıştırılıyor (güvenilirlik/bütünlük kontrol programı).
{% endhighlight %}

Bu işlem tamamlandığında OSSEC'in **rootkit'i** çalıştırmasını isteyip istemediğiniz sorulacak.

{% highlight bash %}
3.3- rootkit tespit etme motorunun çalışmasını ister misiniz? (e/h) [e]: e
- rootcheck çalıştırılıyor (rootkit tespit etme).
{% endhighlight %}

Evet deyip bu adımı da geçin. Sıradaki adım OSSEC'in güzel özelliklerinden biri olan **etkin yanıt üretmenin seçimidir**. Etkin yanıt üretme ile sistemde tanımladığınız kurallara aykırı bir olay meydana geldiğinde, tanımladığınız kurallar çerçevesinde OSSEC’in bu olaylara anında yanıt vermesi sağlanmakta. Bu yüzden bu özelliği etkinleştirin.

{% highlight bash %}
3.4- Etkin yanıt üretme (Active response), edinilen olay
       bilgilerine göre belirli bir komut çalıştırmanıza olanak
       tanır. Örneğin bir IP adresinin engelleyebilir veya bir
       kullanıcının erişimini kısıtlayabilirsiniz.
       Daha fazla bilgi:
       http://www.ossec.net/en/manual.html#active-response
- Etkin yanıt üretmenin (Active response) etkin kılınmasını ister misiniz? (e/h) [e]: e
{% endhighlight %}

Etkinleştirdiğinizde size seçmeniz için ön tanımlı güvenlik duvarı kurallarını sunacaktır.

{% highlight bash %}
 - Etkin yanıt üretme (Active response) etkin kılındı.
   - Öntanımlı olarak host-deny ve firewall-drop etkin yanıt
     mekanizmalarını etkin hale getirebiliriz. Bunlardan ilki
     bir bilgisayarı /etc/hosts.deny dosyasına ekler, ikincisi
     bilgisayarı iptables (linux) veya ipfilter (Solaris,
     FreeBSD vb.) ile engeller.
   - Bunlar, SSHD kaba güç saldırılarını, port taramalarını
     ve diğer saldırı şekillerini durdurmak için kullanılabilir.
     Ayrıca snort olaylarını değerlendirerek engelleme yapmak
     için de ekleyebilirsiniz.
   - firewall-drop yanıtının etkin kılınmasını ister misiniz? (e/h) [e]: e
{% endhighlight %}

Seçip devam edin.

{% highlight bash %}
-- Uyarı seviyesi >= 6 için firewall-drop etkin kılındı (yerel)
   - Etkin yanıt üretme için öntanımlı beyaz liste:
      - 127.0.1.1
{% endhighlight %}

Eklemek istediğiniz başka IP varsa burada ekleyebilirsiniz.

{% highlight bash %}
- Beyaz listeye başka IP adreslerini de eklemek ister misiniz? (e/h)? [h]: h
{% endhighlight %}

Sırada OSSEC'in **hangi kayıt dosyalarını izleyeceğini** seçmekte.

{% highlight bash %}
3.6- Bu dosyaları incelemek için yapılandırma oluşturuluyor:
    -- /var/log/auth.log
    -- /var/log/syslog
    -- /var/log/dpkg.log
    -- /var/log/nginx/access.log (apache log)
    -- /var/log/nginx/error.log (apache log)
{% endhighlight %}

OSSEC'in ön tanımlı olarak seçtiği dosyaların yanı sıra ekleyeceğiniz dosyaları burada eklemek yerine daha ilerde nasıl ekleneceğini göstereceğim.

{% highlight bash %}
- Eğer başka bir dosyayı daha gözlemek isterseniz
   ossec.conf dosyasına yeni bir localfile girdisi ekleyin.
   Yapılandırma hakkındaki herhangi bir sorunuzu cevaplamak için
   http://www.ossec.net adresini ziyaret edebilirsiniz.
{% endhighlight %}

Devam ettiğinizde OSSEC kuruluma geçecektir.

{% highlight bash %}
- Sistem Debian (Ubuntu or derivative).
 - OSSEC HIDS'i önyüklemede başlatmak için başlangıç betiği değiştirildi.
 - Yapılandırma doğru olarak tamamlandı.
 - OSSEC HIDS'i başlatmak için:
		/var/ossec/bin/ossec-control start
 - OSSEC HIDS'i durdurmak için:
		/var/ossec/bin/ossec-control stop
 - Yapılandırma buradan görülebilir veya değiştirilebilir: /var/ossec/etc/ossec.conf
    OSSEC HIDS kullandığınız için teşekkürler.
    Sorularınız, önerileriniz olursa veya her hangi bir yanlış
    bulursanız contact@ossec.net adresi ile veya kamuya açık
    e-posta listemiz ile ossec-list@ossec.net adresinden iletişime
    geçiniz.
    ( http://www.ossec.net/main/support/ ).
    http://www.ossec.net adresinde daha fazla bilgi bulunabilir.
{% endhighlight %}

Böylece kurulumu bitirmiş olacaksınız. OSSEC'i başlatmak için aşağıdaki komutu kullanabilirsiniz.

{% highlight bash %}
sudo /var/ossec/bin/ossec-control start
Starting OSSEC HIDS v2.8 (by Trend Micro Inc.)...
Started ossec-maild...
Started ossec-execd...
Started ossec-analysisd...
Started ossec-logcollector...
Started ossec-syscheckd...
Started ossec-monitord...
Completed.
{% endhighlight %}

*“Completed”* ifadesini gördüğünüzde OSSEC sorunsuzca başlamıştır.

[1]: https://ossec.github.io/downloads.html
[md5]: http://wiki.ubuntu-tr.net/index.php?title=Md5_kontrol%C3%BC
[SHA256]: https://help.ubuntu.com/community/HowToSHA256SUM