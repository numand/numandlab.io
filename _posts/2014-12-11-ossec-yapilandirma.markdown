---
layout: post
title: "OSSEC"
subtitle: "Yapılandırma: Yeni Kayıt Dosyası Ekleme ve Çözücü ve Kural Yazma"
summary: "OSSEC'in takip edip etmesini istediğimiz kayıtları (log) ekleyip, bu kayıtlardan neyi nasıl bize bildirmesi gerektiğini yazı dizisinin bu bölümde anlatmaktayım.  "
part: "Bölüm 4"
date: 2014-12-11
categories: 
- linux
- hids
---

## Yapılandırma

<span class="run-in">Ossec'i genel hatlarıyla</span> öğrendiğimize göre sistemimize uygun olarak yapılandırmaya geçebiliriz.

### Yeni Kayıt Dosyası Ekleme

Ossec'in hangi kayıt dosyalarını takip etmesi gerektiğine artık karar verebiliriz. Kurulum aşamasında, Ossec sistemi tarayarak bazı kayıt dosyalarını takip edeceğini bildirmişti. Ossec'in bulduğu kayıt dosyalarından başka bir dosya girmek için `/var/ossec/etc/ossec.conf` dosyasını açıp aşağıdaki satırları `<!-- Files to monitor (localfiles) -->` ifadesinden sonra girebilirsiniz.

{% highlight xml %}
<localfile>
  <log_format>syslog</log_format>
  <location>/var/log/bir_dosya.log</location>
</localfile>
{% endhighlight %}

Fakat takip edilmesini istediğiniz birçok dosya bulunduğunda her birinin elle girilmesi zor olacaktır. Bunun yerine `/var/ossec/bin/util.sh` betiğini kullanmak daha verimlidir. Betiği aşağıdaki şekilde kullanabilirsiniz.

{% highlight bash %}
sudo /var/ossec/bin/util.sh addfile /var/log/bir_dosya.log
{% endhighlight %}

Eklemek istediğiniz kayıt dosyalarını ekledikten sonra Osssec'i yeniden başlatın.

{% highlight bash %}
sudo /var/ossec/bin/ossec-control restart
{% endhighlight %}

### Çözücü Yazma

Ossec, kayıt dosyalarını belirli süreçlerden geçirerek yorumlanabilecek hale getirmekte ve bu halini kayıt dosyasındaki kaydın bir sorun olup olmadığını kurallarla karşılaştırarak belirlemekte. Yorumlama kısmı `/var/ossec/etc/decoder.xml` dosyasında belirtilen kurallara göre yapılmakta. Dosyayı açıp incelediğinizde birçok uygulama için çözücülerin bulunduğunu göreceksiniz. Bu da kullanıcıya çoğu zaman kendi çözücüsünü yazma zahmetinden kurtarmaktadır.

Bu dosyadan *sshd* için yazılan bir örneği inceleyelim.

{% highlight xml %}
<decoder name="sshd">
  <program_name>^sshd</program_name>
</decoder>
<decoder name="sshd-success">
  <parent>sshd</parent>
  <prematch>^Accepted</prematch>
  <regex offset="after_prematch">^ \S+ for (\S+) from (\S+) port </regex>
  <order>user, srcip</order>
  <fts>name, user, location</fts>
</decoder>
{% endhighlight %}

İlk üç satır, *sshd* için yazılacak diğer çözücülerin sshd grubuna dâhil olmasını sağlayacak ana (`<parent>`) grubu oluşturmakta. Sonraki gelen satırlar, yapılan bir SSH bağlantısından alınan bilgilerin nasıl çözüleceğini tanımlamakta ve bu satırların amacı kullanıcı adı ile IP'sini elde etmektir. Çözücü adı (`<decoder name>`) olarak *sshd-success* kullanılmış ve çözücü `<parent>` değişkeni kullanılarak *sshd* grubuna dâhil edilmiş. `<prematch>` değişkeni, kayıt dosyasından çözücünün hangi ifadeyi araması gerektiğini belirtmektedir. Bu değişkenin yaptığı işi

{% highlight bash %}
grep “Accepted” /var/log/syslog
{% endhighlight %}

komutuna benzetebilirsiniz. Değişken içinde kullanılan **'^'** düzenli ifadesi çözülecek kaydın *“Accepted”* ifadesinden başlayacağını belirtmekte. Eğer kayıt dosyasındaki ifade

{% highlight bash %}
sshd[8813]: Accepted password for root from 192.168.10.1 port 1066 ssh2
{% endhighlight %}

yukarıdaki gibi ise, *“Accepted”* ifadesinden önce gelenler – sshd[8813]: - yok sayılacaktır. Ossec'in kullandığı diğer düzenli ifadelere [bu][1] adresten ulaşabilirsiniz. `<regex>` satırı, kayıt dosyasından `<prematch>` ile yakalanan satırlarının parça parça nasıl çözüleceğini belirlemekte. `<offset>` ile `<prematch>` değişkeninin değerinden sonra gelen ifadelerin dikkate alınması gerektiği belirtilmekte. '`\S`' ifadesi, *“Accepted”* ifadesinden sonra gelen diziyi *(password)* ; '`(\S+)`' ifadesi kullanıcı adını *(root)* ve sonrasında gelen '`(\S+)`' ifadesi ise IP'yi *(192.168.10.1)* ifade etmekte. Parantez içine alınan her düzenli ifade kurallarda kullanılabilir.

Böylece kayıt dosyasından çekilen satır parçalarına ayrılmış bulunmakta. Bu parçalardan kullanıcı adı ve IP'sini almak amaçlandığı için `<order>` satırında bu değerlere karışık gelen değişkenler eklenmiştir. `<regex>` deki ilk '`(\S+)`' ifadesi *'user'* değişkenine, diğeri ise *'srcip'* değişkenine atanmakta. `<fts>` satırında ise, kayıt dosyasında bu çözücünün kurallarına uyan ilk olayın ne zaman gerçekleştiği kaydedilmekte ve olayın hangi çözücüden geldiği *(name)*, olayı gerçekleştiren kullanıcının adı *(user)* ve olayın hangi kayıt dosyasından elde edildiği *(location)* bilgileri sunulmakta. Çözücü yazım kuralları hakkında daha fazla bilgi edinmek için [bu][2] adrese başvurabilirsiniz.

Bu bilgiler ışığında kendimiz için bir çözücü yazalım. Bu çözücü için Lynis uygulamasının kayıt dosyası olan */var/log/lynis.log* kullanılacaktır. Dosyadaki

{% highlight bash %}
[22:47:28] Warning: Found one or more vulnerable packages. [PKGS-7392]
{% endhighlight %}

satırı kullanılacak. Çözücü ismi olarak *lynis-vulnerable-packages* uygun olacaktır. İlk yapılması gereken, kayıttan hangi değerlerin alınacağının tespit edilmesidir. Bu satırdan *"vulnerable"* ve *"\[PKGS-7391\]"* değerleri çekilecek. Bundan sonra yapmanız gereken bu değerleri hangi değişkene atamanız gerektiğine karar vermek olacaktır. İlk değer `extra_data` değişkenine; ikinci değer `id` değişkenine atanacak. Hangi değerin hangi değişkene atanacağının kararından sonra kayıt dosyasından çözücünün hangi değeri araması gerektiğini belirtmek olacaktır. Bu kayıt için *"\[22:47:28\] Warning:"* değeri seçilmiştir. Buna göre çözücüyü yazmaya başlayabiliriz.

`[22:47:28]`: Ossec düzenli ifadelerinden '`\d`', rakamları göstermek için kullanılabilir. Köşeli parantezleri ve iki nokta üst üste işaretini '`\p`' düzenli ifadesiyle gösterebiliriz. Bu durumda

{% highlight xml %}
[22:47:28]: = \p\d\d\p\d\d\p\d\d\p
{% endhighlight %}

şeklinde ifade edilebilir. Fakat Ossec, köşeli parantez ve iki nokta üst üste işaretini olduğu gibi gösterdiğinizde de tanıyacaktır ve bunları olduğu gibi kullanmak çözücüyü okumayı ve yazmayı kolaylaştırmaktadır. Bu yüzden yukarıdaki eşitlik

{% highlight xml %}
[22:47:28]: = [\d\d:\d\d:\d\d]
{% endhighlight %}

şeklinde yazılabilir.

`Warning:`: Ossec'in kayıt dosyalarını okurken bu değeri aramasını istediğimizden bu değer için herhangi bir şey yazmamıza gerek yok. Böylece Ossec'in ne araması gerektiğini belirlemiş olduk.

{% highlight xml %}
<decoder name="lynis-vulnerable-packages">
<prematch>[\d\d:\d\d:\d\d] Warning:</prematch>
{% endhighlight %}

Kaydın diğer kısımlarından hangi değerleri alacağımızı belirlemiştik. Bu değerlerin dışındaki tüm değerleri Ossec düzenli ifadeleriyle gösterebiliriz. Geriye kalan değerler harflerden oluştuğundan bunları '`\w`' düzenli ifadesiyle gösterebiliriz. Her harf için '*\w*' yazmaktansa, düzenli ifade sonuna konacak '`+`' ifadesiyle düzenli ifadenin birden fazla harfi karşılamasını sağlayabiliriz. Kayıttan değerini çekmek istediğimiz ifadeler için '`\S`' düzenli ifadesini kullanırsak,

{% highlight bash %}
Found one  or  more vulnerable packages. [PKGS-7391]
\w+   \w+  \w+ \w+  (\S+)      \w+       (\S+)
{% endhighlight %}

satırı yukarıdaki gibi ifade edebiliriz. Artık çözücüye son halini verebiliriz.

{% highlight xml %}
<decoder name="lynis-vulnerable-packages">
  <prematch>[\d\d:\d\d:\d\d] Warning:</prematch>
  <regex offset="after_prematch">^ \w+ \w+ \w+ \w+ (\S+) \w+. (\S+)$</regex>
  <order>extra_data, id</order>
  <fts>location</fts>
</decoder>
{% endhighlight %}

Kendi oluşturacağınız çözücüleri `/var/ossec/etc/local_decoder.xml` dosyasına kaydetmelisiniz. */var/ossec/etc/decoder.xml* dosyasına yazacağınız çözücüler güncelleme sırasında yitirilecektir. `local_decoder.xml`, ön tanımlı olarak mevcut bulunmadığından önce dosyanın oluşturulması gerekmekte. Çözücüyü kaydettikten sonra çalışıp çalışmadığını `/var/ossec/bin/ossec-logtest` komutuyla denetleyebilirsiniz.

{% highlight bash %}
sudo /var/ossec/bin/ossec-logtest
{% endhighlight %}

komutunu verdikten sonra

{% highlight bash %}
ossec-testrule: Type one log per line.
{% endhighlight %}

İfadesini gördüğünüzde kaydı olduğu gibi yazın. Sonuç olarak

{% highlight bash %}
**Phase 1: Completed pre-decoding.
       full event: '[22:47:28] Warning: Found one or more vulnerable packages. [PKGS-7392]'
       hostname: 'linux'
       program_name: '(null)'
       log: '[22:47:28] Warning: Found one or more vulnerable packages. [PKGS-7392]'

**Phase 2: Completed decoding.
       decoder: 'lynis-vulnerable-packages'
       extra_data: 'vulnerable'
       id: '[PKGS-7392]'
[22:47:28] Warning: Found one or more vulnerable packages. [PKGS-7392]
{% endhighlight %}

şeklinde bir çıktı alacaksınız. Çıktıda da görüldüğü üzere çözücü olarak yazdığımız çözücünün adı "*lynis-vulnerable-packages*"; `extra_data` değişkeninin değeri olarak *“vulnerable”* ve `id` değişkeninin değeri olarak *"[PKGS-7392]"* görünmekte.

### Kural Yazma

Çözücülerin elde ettiği bilgilerin kullanıcıya daha anlamlı bir şekilde ifade edilerek ulaşmasını ve kullanıcının uyarılmasını Ossec kurallarla sağlamaktadır. `/var/ossec/rules` dizini altında ön tanımlı çözücüler için yazılmış birçok kural bulunmaktadır. Kullanıcının kendi kurallarını yazması için aynı dizin içinde `local_rules.xml` dosyası barınmaktadır. Bu dosyaya bir önceki maddede oluşturduğumuz çözücü için bir kural yazalım. Yazmaya başlamadan önce Ossec kural yazım kurallarını [bu][3] adresten okumanızı öneririm.

*local_rules.xml* dosyasını açtığınızda ön tanımlı bazı kurallar ve  bu kuralların gruplandırıldığını (`<group>`) göreceksiniz. Biz de yazacağımız kuralı gruplandırmak zorundayız. Oluşturacağımız gruba lynis adını verelim ve yerel bir kural olduğu için '`local`' etiketini de ekleyelim. Kural yazımının, çözücü yazımından daha kolay olduğunu açtığınız dosya içindeki kurallardan anlayabilirsiniz. İhtiyacımız olan yazacağımız kural için bir **kural numarası** (`<rule id>`) ve **kural seviyesi** (`level`) belirlemek ve bu kuralın **hangi çözücüden geçtiğini** (`<decoded_as>`) belirlemektir. Kural seviyelerinin ne ifade ettiğine [bu][4] adresten ulaşabilirsiniz. Yerel kural numaralarının 100.000 ile 119.999 arasında olması gerektiği [bu PDF][5] belgesinde belirtilmektedir. Aynı belgeden hangi uygulamanın kurallarının hangi kural numarası aralığına düştüğü bilgisini de edinebilirsiniz.

Yazacağımız kural için kural numarası olarak 111.111; kural seviyesi olarak 12 seçilmiştir. Kuralın uyarı olarak “Açık barındıran paketler var!” cümlesini bastırması (`<description>`) istenmiştir. Bu bilgiler ışığında kuralımız

{% highlight xml %}
<group name="local,lynis,">
  <rule id="111111" level="12">
    <decoded_as>lynis-vulnerable-packages</decoded_as>
    <description>Açık barındıran paketler var!</description>
  </rule>
</group>
{% endhighlight %}

halini alacaktır. Çözücü yazımı sırasında parantez içine alınan her düzenli ifadenin kurallarda kullanılabilineceğini söylemiştik. Yazdığımız çözücüde parantez içindeki düzenli ifadeleri sırasıyla '`extra_data`' ve '`id`' değişkenlerine atamıştık. Bunun gibi, *'extra_data'* ve *'id'* değişkenlerine daha başka değerler atanmış olabilir. Bu yüzden kuralımıza hangi *'extra_data'* ve *'id'* değişkenlerini okuması gerektiğini söyleyebiliriz. Bunun için yapmanız gereken değişkenleri ve değerlerini `<></>` ifadesi içinde kullanmak olacaktır. Örneğimiz üzerinde gösterecek olursam

{% highlight xml %}
<group name="local,lynis,">
  <rule id="111111" level="12">
    <extra_data>vulnerable</extra_data>
    <id>[PKGS-7391]</id>
    <decoded_as>lynis-vulnerable-package</decoded_as>
    <description>Açık barındıran paketler var!</description>
  </rule>
</group>
{% endhighlight %}

şeklinde olacaktır.

Kural düzenleme kısmında Ossec'in bazı ön tanımlı kurallarına dair tavsiyelerde bulunmak istiyorum. Ön tanımlı olarak Ossec, sistemi 22 saatte (79200 saniye) bir taramaktadır. Bu süreyi daha aza indirmek için `ossec.conf` dosyasındaki aşağıdaki satırı istediğiniz bir değerle değiştirin.

{% highlight xml %}
<!-- Frequency that syscheck is executed - default to every 22 hours -->
<frequency>79200</frequency>
{% endhighlight %}

Değiştirmek isteyeceğiniz bir diğer ayar ise Ossec'in hangi dizinleri ve bu dizinleri nasıl denetleyeceği olabilir. Ön tanımlı olarak Ossec `/etc`, `/usr/bin`, `/usr/sbin`, `/bin` ve `/sbin` dizinlerini denetlemektedir. Bu dizinler sistem için önemli ikilikleri ve dosyaları barındırdığı için bunların **gerçek zamanlı (realtime)** olarak denetlenmesi daha makul görünmekte. Bu yüzden `ossec.conf` dosyası içindeki aşağıdaki satırları

{% highlight xml %}
<!-- Directories to check (perform all possible verifications) -->
<directories check_all="yes">/etc,/usr/bin,/usr/sbin</directories>
<directories check_all="yes">/bin,/sbin</directories>
{% endhighlight %}

aşağıdaki şekilde değiştirebilirsiniz.

{% highlight xml %}
<!-- Directories to check  (perform all possible verifications) -->
<directories realtime="yes" check_all="yes">/etc,/usr/bin,/usr/sbin</directories>
<directories realtime="yes" check_all="yes">/bin,/sbin</directories>
{% endhighlight %}

Bu dizinlerin yanı sıra bir sunucu için önemli olan diğer dizin ise siteye ait dosyaların bulunduğu dizindir.  Bu dizinin de gerçek zamanlı olarak denetlenmesini ve dizin içinde gerçekleşen değişikliklerin raporlanmasını (`report_changes`) sağlamak için gerekli parametrelerle birlikte dizinin eklenmesi yeterlidir. Fakat dizin içinde birçok dosya bulunacaktır ve bu dosyaların hepsinin gerçek zamanlı olarak takip edilmesi sunucuya yük getirecektir. Bu yüzden takip edilmesi istenen dosya türlerinin belirtilmesi yerinde olacaktır. Bunu da '`restrict`' parametresiyle sağlayabilirsiniz. Site dosyalarının /var/www/site dizini altında bulunduğu ve PHP, HTML, CSS, JS, ve XML dosya uzantılarına sahip dosyaların takip edileceği varsayıldığında, aşağıdaki gibi bir girdi oluşturabilirsiniz.

{% highlight xml %}
<directories realtime="yes" report_changes="yes" restrict=".xml|.php|.html|.js|.css">/var/www/site/</directories>
{% endhighlight %}

Sunucuya eklenen dosyaların sizin tarafınızdan mı yoksa kötü amaçlı bir yazılım tarafından ya da sızma girişimi ardından yapılıp yapılmadığını anlamak için eklenen yeni dosyaların bildiriminin sağlanması da önemli değişikliklerden biridir. Bunu sağlamak için `ossec.conf` dosyasına aşağıdaki girdileri eklemelisiniz.

{% highlight xml %}
<alert_new_files>yes</alert_new_files>
<scan_on_start>no</scan_on_start>
<auto_ignore>no</auto_ignore>
{% endhighlight %}

`<alert_new_files>` değişkeni ile eklenen yeni dosyalar bildirilecektir. `<scan_on_start>` değişkenine hayır denilerek syscheck'in Ossec başlar başlamaz dosya bütünlük denetimi yapması önlenmiş olundu ki böylece işlemci aşırı kullanılmayacaktır. `<auto_ignore>` değişkenine de hayır denilerek *syscheck'in* sık değişen dosyaları atlamasının önüne geçilmiş olundu.

Eklenen yeni dosyaların bildirimini sağlamak için `/var/ossec/rules/local_rules.xml` dosyasını düzenlememiz gerek. Aslında `/var/ossec/rules/ossec_rules.xml` dosyasında yeni eklenen dosyaların bildirimi kapalıdır ve dosya istenilen şekilde düzenlenebilir fakat bu dosya Ossec güncellemesi sırasında ön tanımlı değerlere göre değiştiği için dosya üzerinde değişiklik yapmamalısınız.

{% highlight xml %}
<rule id="554" level="7" overwrite="yes">
  <category>ossec</category>
  <decoded_as>syscheck_new_entry</decoded_as>
  <match>/var/www/site</match>
  <description>/var/www/site dizinine dosya eklenedi.</description>
  <group>syscheck</group>
</rule>
{% endhighlight %}

`<match>` değişkeni ile sunucuya eklenen her dosyanın değil, sadece değişkenle tanımlanan dizindeki dosyaların bildiriminin yapılması sağlanmış oldu.

Bunlara benzer kendi kurallarınızı oluşturmanıza yardımcı olacak belgeleri [bu][6] bağlantıda bulabilirsiniz.

[1]: https://ossec-docs.readthedocs.io/en/latest/syntax/regex.html#os-regex
[2]: https://ossec-docs.readthedocs.io/en/latest/syntax/head_decoders.html
[3]: https://ossec-docs.readthedocs.io/en/latest/syntax/head_rules.html
[4]: https://ossec-docs.readthedocs.io/en/latest/manual/rules-decoders/rule-levels.html
[5]: http://www.ossec.net/ossec-docs/OSSEC-book-ch4.pdf
[6]:https://ossec-docs.readthedocs.io/en/latest/syntax/index.html