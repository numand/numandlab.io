---
layout: post
title: "OSSEC"
subtitle: "Dizin Yapısı, Komutlar ve Sunucu ile Aracı İletişimini Sağlama"
summary: "OSSEC için önemli dizinler ile komutların açıklanması ve sunucu ile aracının bir birleriyle nasıl iletişim kuracaklarının gösterimi  "
part: "Bölüm 3"
date: 2014-12-04
categories: 
- linux
- hids
---

## Dizin Yapısı ve Komutlar

<span class="run-in">Kurulumu tamamladıktan sonra</span> OSSEC'in neler sağladığına bakalım.

`active-response` dizini içinde OSSEC'in sunduğu betikler bulunmakta. Bu betikleri tanımlayacağınız bir olay karşısında çalışması için ayarlayabilirsiniz.

`agentless` dizini, güvenlik duvarı ve yönlendiriciler gibi üzerine aracısız kurulum yapılan sistemler üzerinde gerçekleştirilecek işlemlere dair betikler ve ayarlamaları barındırmakta.

`bin` dizini, sunucu ve aracının kullandığı ikilik dosyaları barındırmakta. Bunlardan biri olan `ossec-control` komutunu OSSEC'i başlatmak için kullanmıştık. Bunlardan bazılarını tanıyalım:

| Komut                                   | Görevi                                                                                                                                |
| :--------------------------------|:-------------------------------------------------------------------------------------------------- |
| `ossec-analysisd`                | Bütün analizleri yapan ana süreçtir.                                                                                |
| `ossec-remoted`                 | Uzak sunucudan aracıların gönderdiği bilgileri alan süreçtir.                                       |
| `ossec-logcollector`           | Kayıt dosyalarını okuyan süreçtir.                                                                                    |
| `ossec-agentd`                   | Kayıt dosyalarını sunucuya gönderen süreçtir.                                                               |
| `ossec-maild`                    | Kayıt dosyalarını okuyan süreçtir.                                                                                   |
| `ossec-logcollector`          | E-posta aracılığıyla kullanıcıyı bilgilendiren süreçtir.                                                     |
| `ossec-execd`                   | Etkin yanıtları çalıştıran süreçtir.                                                                                     |
| `ossec-monitord`              | Aracıların durumlarını gözleyen, kayıt dosyalarını imzalayan ve sıkıştıran süreçtir.   |

Bu süreçlerin çoğu yetkisiz kullanıcılar ile ve chroot ortamında çalışmakta.

| Komut                 | Kullanıcı           | chroot  |
| :------------------- |:----------------- | :-------- |
| ossec-analysisd   | ossec               | Evet      |
| ossec-remoted    | ossecr             | Evet      |
| ossec-maild         | ossecm           | Evet      |
| ossec-agentd      | ossecr              | Evet      |

Bu yapı OSSEC'i daha güvenli kılmakta. Her bir ikiliğin ne görev yaptığını öğrenmek için bu bağlantıya bakabilirsiniz.

`etc`, OSSEC'in ayar dosyalarının barındırıldığı dizindir. Dizinde bulunan `ossec.conf` dosyası OSSEC'in genel ayarlarını belirlemekte. `decoder.xml` kayıt dosyalarının nasıl yorumlanacağının belirlendiği dosyadır.

`logs`, OSSEC'in kayıt dosyalarını yorumladıktan sonra belirlediği sorunları, kullanıcı tanımlı ve ön tanımlı kurallara aykırı gerçekleşen olanları, dosya bütünlüklerinde meydana gelen değişiklikleri, rootkit sonuçlarını vs. kayıt altına aldığı dizindir.

`rules` dizini sistemde meydana gelen olaylara karşı nasıl bir yanıt üretilmesi ve bunların kullanıcıya nasıl sunulması gerektiğine dair kuralların bulunduğu dizindir. Ön tanımlı kuralların dışında yazılmak istenen kurallar `local_rules.xml` dosyasına yazılabilir.

`stats` dizininde elde edilen bilgilere dair istatistikler barındırılmaktadır.

## Sunucu ve Aracı İletişiminin Sağlanması

Sunucu ve aracının farklı makinelerde bulunması istenilen durumlarda, sunucu olarak kullanılacak makineye kurulum türünü sunucu seçerek; aracı olacak makineye ise kurulum türünü aracı seçerek kurulum yapılabilir. Kurulum adımları, sunucu için yukarıda gösterilen adımlardan farklı olmamakla birlikte etkin yanıt adımında aracı olarak kullanacağınız makinenin IP adresini beyaz listeye ekleyebilirsiniz. Aracı kurulumu sırasında burada anlatılan kurulumdan farklı olarak sizden OSSEC'in kurulu olduğu sunucunun IP adresini girmeniz istenecektir. Daha önce var olan bir kurulum üzerine yeniden kurulum yapabilir ve kurulum türünü değiştirebilirsiniz. Böyle bir durumda sunucu ile aracı arasında kullanılan anahtar dosyası haricinde diğer bilgiler değişmeden kalacaktır.

Kurulum bittikten sonra iki makinenin iletişim kurabilmesi için gerekli olan anahtarı sunucu üzerinde üretip bu anahtarı aracıya yüklemeniz gerekmektedir. Anahtar üretimi için sunucu üzerinde aşağıdaki komutu yürütün.

{% highlight bash %}
sudo /var/ossec/bin/manage_agents
{% endhighlight %}

Aşağıdaki seçeneklerden aracı ekleme seçeneğini (A) seçin.

{% highlight bash %}
****************************************
* OSSEC HIDS v2.8 Agent manager.     *
* The following options are available: *
****************************************
   (A)dd an agent (A).
   (E)xtract key for an agent (E).
   (L)ist already added agents (L).
   (R)emove an agent (R).
   (Q)uit.
Choose your action: A,E,L,R or Q:A
{% endhighlight %}

Aracı için bir isim, aracının IP adresini ve ID'sini girin.

{% highlight bash %}
- Adding a new agent (use '\q' to return to the main menu).
  Please provide the following:
* A name for the new agent: uzak1
* The IP Address of the new agent: 1.2.3.4.
* An ID for the new agent[001]: 001
{% endhighlight %}

Girdiğiniz bilgiler doğru ise devam edin.

{% highlight bash %}
Agent information:
   ID:001
   Name:uzak1
   IP Address:1.2.3.4
Confirm adding it?(y/n): y
{% endhighlight %}

Bir sonraki adımda aracı için bir anahtar üretme seçeneğini (E) seçin.

{% highlight bash %}
****************************************
* OSSEC HIDS v2.8 Agent manager.     *
* The following options are available: *
****************************************
   (A)dd an agent (A).
   (E)xtract key for an agent (E).
   (L)ist already added agents (L).
   (R)emove an agent (R).
   (Q)uit.
Choose your action: A,E,L,R or Q: E
{% endhighlight %}

Aracının ID'sini girin.

{% highlight bash %}
Available agents:
   ID: 001, Name: uzak1, IP: 192.168.2.165
Provide the ID of the agent to extract the key (or '\q' to quit): 001
{% endhighlight %}

ID'yi girdikten sonra size aracı için üretilen anahtar sunulacaktır.

{% highlight bash %}
Agent key information for '001' is:
MD....akd==
{% endhighlight %}

Anahtarı ürettikten sonra aracının kurulu olduğu makineye dönerek `/var/ossec/bin/manage_agent` komutunu çalıştırın ve sunucudan anahtar yükleme seçeneğini seçin.

{% highlight bash %}
****************************************
* OSSEC HIDS v2.8 Agent manager.     *
* The following options are available: *
****************************************
   (I)mport key from the server (I).
   (Q)uit.
Choose your action: I or Q: I
{% endhighlight %}

Anahtarı girin.

{% highlight bash %}
* Provide the Key generated by the server.
* The best approach is to cut and paste it.
*** OBS: Do not include spaces or new lines.
Paste it here (or '\q' to quit): MD...akd==
{% endhighlight %}

Anahtarı girdikten sonra anahtar sahibi aracının bilgileri ekrana basılacaktır. Doğru ise evet deyip devam edin.

{% highlight bash %}
Agent information:
   ID:001
   Name:uzak1
   IP Address:1.2.3.4
Confirm adding it?(y/n): y
Added.
{% endhighlight %}

Aracının kurulu olduğu makinede `/var/ossec/etc/ossec.conf` dosyası içerisinde

{% highlight XML %}
<client>
  <server-hostname>5.6.7.8</server-hostname>
</client>
{% endhighlight %}

satırının olup olmadığını denetleyin. Buradaki IP adresi, sunucunun IP adresi olmalıdır.

Eğer bütün adımları doğru tamamladıysanız

{% highlight xml %}
sudo /var/ossec/bin/ossec-control restart
{% endhighlight %}

komutunu hem sunucu hem de aracı tarafında yürütün. Aracı ve sunucunun UDP 1514 portu üzerinden iletişim kurabilmesi için güvenlik duvarı gibi uygulamalar tarafından portun engellenmediğinden emin olun.