---
layout: post
title: "Markdown Demo"
subtitle: "Examples of Markdown Syntax"
summary: "Demonstrating how Markdown syntax looks like on the site "
date: 2016-09-02
math: "yes"
language: "english"
categories: 
- jekyll
- markdown
---

This is Markdown cheat sheet demo for **Beste**, this Jekyll theme. Please check the raw content of this file for the markdown usage.

kramdown supercharges Markdown with some interesting features. [kramdown](https://kramdown.gettalong.org/index.html) is the default Jekyll Markdown processor. When creating your site with Jekyll, you can use the standard Markdown syntax plus some specific kramdown syntax. Jekyll would render your Markdown/kramdown into HTML.

## Paragraph

The first paragraph.

Another paragraph

This is a paragraph  
which contains a hard line break.

## Headers

First level header
==================

Second level header
-------------------

# H1 header

## H2 header

### H3 header

#### H4 header

##### H5 header

###### H6 header


## Blockquotes

A blockquote is started using the ```>``` marker followed by an optional space; all following lines that are also started with the blockquote marker belong to the blockquote. You can use any block-level elements inside a blockquote:

> A sample blockquote.
>
> >Nested blockquotes are
> >also possible.
>
> ### Headers work too
> This is the outer quote again.
> Also, you can *use* **Markdown** in a blockquote.

## Code Blocks and Syntax Highlighting

kramdown supports two different code block styles. One uses lines indented with either four spaces or one tab whereas the other uses lines with tilde characters as delimiters – therefore the content does not need to be indented:

    This is a sample code block.

    Continued here.

~~~~~~
This is also a code block.
~~~
Ending lines must have at least as many tildes as the starting line.
~~~~~~~~~~~~

The following is a code block with a language specified:

~~~ ruby
def what?
  42
end
~~~

Jekyll uses the [Rouge Ruby library][rouge] for syntax highlighting. For a list of supported languages visit the Rouge website. For syntax highlighting, use [Liquid] tags.

{% raw %}
    {% highligh name_of_programming_language linenos %}

    code goes here

    {% endhighlight %}
{% endraw %}

All codes will use light color scheme of Solarized color palette. Here is an examples:

{% highlight ruby linenos %}
# Ruby: Say hi to everybody
def say_hi
  if @names.nil?
    puts "..."
  elsif @names.respond_to?("each")
    # @names is a list of some kind, iterate!
    @names.each do |name|
      puts "Hello #{name}!"
    end
  else
    puts "Hello #{@names}!"
  end
end
{% endhighlight %}

This sentence has an `inline code` in it. If you want to use literal backticks in your code, just use two or more backticks as delimiters. The space right after the beginning delimiter and the one right before the closing delimiter are ignored:

~~~
    Use backticks to markup code, e.g. `` `code` ``.
~~~

## Lists

kramdown supports ordered and unordered lists. Ordered lists are started by using a number followed by a period, a space and then the list item text. The content of a list item consists of block-level elements. All lines which have the same indent as the text of the line with the list marker belong to the list item:

1. First ordered list item
2. Another item
1. Actual numbers don't matter, just that it's a number
    1. Ordered sub-list
4. And another item.

* Unordered lists that use asterisks
- Or minuses
+ Or pluses
* will be shown with a dot.

1.  This is a list item with two paragraphs. The quick,
     brown fox jumps over a lazy dog. DJs flock by
     when MTV ax quiz prog.

    Junk MTV quiz graced by fox whelps. Bawds jog,
    flick quartz, vex nymphs. Waltz, bad nymph, for
    quick jigs vex! Fox nymphs grab quick-jived waltz.

2.  Brick quiz whangs jumpy veldt fox. Bright vixens jump;
     dozy fowl quack.

*   A list item with a blockquote:

    > This is a blockquote
    > inside a list item.

*   A list item with a code block:

        {% highlight bash %}if [[ "${HOME}" == "/home/ubuntu" ]] ; then printf "%s" "OK"; fi{% endhighlight %}

## Horizontal Rules

It is easy to insert a horizontal rule in kramdown: just use three or more asterisks, dashes or underscores, optionally separated by spaces or tabs, on an otherwise blank line:

Asterisks:

***

Dashes:

- - - - -

Underscores:

___

## Definition Lists

A definition list works similar to a normal list and is used to associate definitions with terms. Definition lists are started when a normal paragraph is followed by a line starting with a colon and then the definition text. One term can have many definitions and multiple terms can have the same definition. Each line of the preceding paragraph is assumed to contain one term, for example:

kramdown
  : A Markdown-superset converter

Maruku
  : Another Markdown-superset


## Tables

Tables can be created with or without a leading pipe character: If the first line of a table contains a pipe character at the start of the line (optionally indented up to three spaces), then all leading pipe characters (i.e. pipe characters that are only preceded by whitespace) are ignored on all table lines. Otherwise they are not ignored and count when dividing a table line into table cells.

| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |

**Note**

The row of dashes between the table header and body must have at least three dashes in each column.

By including colons in the header row, you can align the text within that column:

| Left Aligned | Centered | Right Aligned | Left Aligned | Centered | Right Aligned |
| :----------- | :------: | ------------: | :----------- | :------: | ------------: |
| Cell 1       | Cell 2   | Cell 3        | Cell 4       | Cell 5   | Cell 6        |
| Cell 7       | Cell 8   | Cell 9        | Cell 10      | Cell 11  | Cell 12       |


## Emphasis

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~


## Links

A simple link can be created by surrounding the text with square brackets and the link URL with parentheses:

~~~
[Here's an example](https://numand.gitlab.io)
~~~

[Here's an example](https://numand.gitlab.io)

You can also add title information to the link:

~~~
A [link](https://numand.gitlab.io "hp") to the homepage.
~~~

There is another way to create links which does not interrupt the text flow. The URL and title are defined using a reference name and this reference name is then used in square brackets instead of the link URL:

~~~
    A [link][myhomepage] to the homepage.

    [myhomepage]: https://numand.gitlab.io "hp"
~~~

If the link text itself is the reference name, the second set of square brackets can be omitted:

~~~
    A link to the [my homepage].

    [my homepage]: https://numand.gitlab.io "hp"
~~~

## Images

Images can be created in a similar way: just use an exclamation mark before the square brackets. The link text will become the alternative text of the image and the link URL specifies the image source:

~~~
    Site's favicon: ![favicon](/assets/favicon.jpg)
~~~

![favicon](/assets/favicon.jpg)

Images will be scaled according to screen size.

![ubuntu][ubuntu]{: #boxshadow}

## Footnotes

Footnotes can easily be used in kramdown. Just set a footnote marker (consists of square brackets with a caret and the footnote name inside) in the text and somewhere else the footnote definition (which basically looks like a reference link definition):

~~~
    This is a text with a footnote[^1].

    [^1]: And here is the definition.
~~~

This is a text with a footnote[^1].

[^1]: And here is the definition.

The footnote definition can contain any block-level element, all lines following a footnote definition indented with four spaces or one tab belong to the definition:

~~~
This is a text with a footnote[^2].

[^2]:
    And here is the definition.

    > With a quote!
~~~

## Abbreviations

Abbreviations will work out of the box once you add an abbreviation definition. So you can just write the text and add the definitions later on.

~~~
This is an HTML example.

*[HTML]: Hyper Text Markup Language
~~~

This is an HTML example.

*[HTML]: Hyper Text Markup Language

## Sub and Super Scripts

5<sup>3</sup> = 125. Water is H<sub>2</sub>O.

## Math

I prefer to use KaTeX to convert TeX math formulas into KaTeX HTML, eliminating the need for client-side math-rendering. Just put double ```$``` sign before and after the math formula to render it.

The following is a math block:

$$
\begin{aligned}
  & \phi(x,y) = \phi \left(\sum_{i=1}^n x_ie_i, \sum_{j=1}^n y_je_j \right)
  = \sum_{i=1}^n \sum_{j=1}^n x_i y_j \phi(e_i, e_j) = \\
  & (x_1, \ldots, x_n) \left( \begin{array}{ccc}
      \phi(e_1, e_1) & \cdots & \phi(e_1, e_n) \\
      \vdots & \ddots & \vdots \\
      \phi(e_n, e_1) & \cdots & \phi(e_n, e_n)
    \end{array} \right)
  \left( \begin{array}{c}
      y_1 \\
      \vdots \\
      y_n
    \end{array} \right)
\end{aligned}
$$

But next comes a paragraph with an inline math statement: $$ \phi(x,y) = \phi \left(\sum_{i=1}^n x_ie_i, \sum_{j=1}^n y_je_j \right) = \sum_{i=1}^n \sum_{j=1}^n x_i y_j \phi(e_i, e_j) $$

## Table of Contents

On your _config.yaml, define the levels of your toc:

~~~
kramdown:
  ...
  toc_levels: [1, 2, 3]
~~~

Add this to generate table:

~~~
* This line is needed, but won't appear. Replace '*' with '1' to create a numbered list.
{:toc}
~~~

{::comment}
<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
1. TOC
{:toc}
</details>
{:/comment}

1. TOC
{:toc}


## More About Markdown

If you are new to Markdown syntax, here is an exellent [source] from John Gruber, who wrote Markdown. Also, if you want to know more about Kramdown, you can check it's syntax in [here].

[rouge]: http://rouge.jneen.net/
[Liquid]: https://shopify.github.io/liquid/
[ubuntu]: /assets/images/canonical-friends_orange_hex.svg "Ubuntu logo"
[^1]: Let's put the explanation of footnote in here.
[source]: https://daringfireball.net/projects/markdown/syntax
[here]: https://kramdown.gettalong.org/syntax.html
