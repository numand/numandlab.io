---
layout: post
title: "Ubuntu 16.04.6 Üzerine Projet Open Kurulumu"
summary: "Proje yönetim yazılımı olan Project Open'ın Ubuntu 16.04.6 sürümüne kurulumu "
date: 2019-10-07
language: "turkish"
categories:
- linux
- proje yönetimi
- project management
---

## Giriş
<span class="run-out">Proje yönetimi</span> için özgür ya da açık kaynak bir yazılım arayanlar için [Project Open](http://www.project-open.com/index.html) en iyi tercihlerden biridir.

## Kurulum

### projop Kullanıcısının Oluşturulması

`projop` kullanıcısının ev dizinini olurturun:

{% highlight bash %}
mkdir -p /web/projop
{% endhighlight %}

`projop` adında bir grup oluşturun:

{% highlight bash %}
groupadd projop
{% endhighlight %}

Ev dizini `/web/projop` olan ve `projop` grubuna bağlı `projop` adında bir kullanıcı oluşturun:

{% highlight bash %}
useradd -g projop -d /web/projop -m -s /bin/bash projop
{% endhighlight %}

`web/projop` dizinindeki dosyaların kullanıcı ve grup sahipliklerinin atayın:

{% highlight bash %}
chown -R projop:projop /web/projop/
{% endhighlight %}

### Gerekli Paketlerin Kurulumu

Project Open v5.0 kurulumu için aşağıdaki paketlerin depodan kurulması gerekmektedir.

| Paket Adı                | Sürümü                            | İşlevi                                                       |
| ------------------------ | --------------------------------- | ------------------------------------------------------------ |
| git-core                 | 2.7.4-0ubuntu1.6                  | Sürüm Kontrolü                                               |
| unzip                    | 6.0-20ubuntu1                     |                                                              |
| zip                      | 3.0-11                            |                                                              |
| make                     | 4.1-6                             |                                                              |
| wwwconfig-common         | 0.2.2                             |                                                              |
| nginx                    | 1.10.3-0ubuntu0.16.04.4           | Proxy sunucu olarak kullanıp 80 portundan erişimin sağlanması |
| jodconverter             | 2.2.2-8                           |                                                              |
| ldap-utils               | 2.4.42+dfsg-2ubuntu3.6            | LDAP kullanımını sağlar                                      |
| ghostscript              | 9.26~dfsg+0-0ubuntu0.16.04.11     |                                                              |
| gsfonts                  | 8.11+urwcyr1.0.7~pre44-4.2ubuntu1 |                                                              |
| imagemagick              | 8:6.8.9.9-7ubuntu5.14             |                                                              |
| graphviz                 | 2.38.0-12ubuntu2                  |                                                              |
| libcupsimage2            | 2.1.3-4ubuntu0.10                 |                                                              |
| libreoffice-writer       | 5.1.6~rc2-0ubuntu1~xenial10       | Metin dosyalarının dönüştürülmesi                            |
| libreoffice-draw         | 5.1.6~rc2-0ubuntu1~xenial10       | Çizim dosyalarının dönüştürülmesi                            |
| libreoffice-java-common  | 5.1.6~rc2-0ubuntu1~xenial10       |                                                              |
| postgresql-9.5           | 9.5.19-0ubuntu0.16.04.1           |                                                              |
| postgresql-doc-9.5       | 9.5.19-0ubuntu0.16.04.1           |                                                              |
| postgresql-client-9.5    | 9.5.19-0ubuntu0.16.04.1           |                                                              |
| postgresql-client-common | 173ubuntu0.1                      |                                                              |
| postgresql-contrib-9.5   | 9.5.19-0ubuntu0.16.04.1           |                                                              |

Bu paketlerin kurulumu için aşağıdaki komutu kullanın:

{% highlight bash %}
apt install git-core unzip zip make wwwconfig-common nginx jodconverter ldap-utils \
postgresql-9.5 postgresql-doc-9.5 postgresql-client-9.5 postgresql-client-common \
postgresql-contrib-9.5 ghostscript gsfonts imagemagick graphviz libcupsimage2 \
libreoffice-writer libreoffice-draw libreoffice-java-common
{% endhighlight %}

PostgreSQL kurulumunu doğrulayın:

{% highlight bash %}
su - projop
psql --version
exit
{% endhighlight %}

`psql (PostgreSQL) 9.5.19` gibi bir cevap almanız gerekiyor.

### ProjectOpen Kurulum Dosyalarının Temini

{% highlight bash %}
cd /usr/local/src/
wget http://sourceforge.net/projects/project-open/files/project-open/Support%20Files/naviserver-4.99.8.tgz
wget http://sourceforge.net/projects/project-open/files/project-open/V5.0/update/project-open-Update-5.0.2.2.0.tgz
wget http://sourceforge.net/projects/project-open/files/project-open/Support%20Files/web_projop-aux-files.5.0.0.0.0.tgz

cd /usr/local
tar xzf /usr/local/src/naviserver-4.99.8.tgz

su - projop
tar xzf /usr/local/src/project-open-Update-5.0.2.2.0.tgz
tar xzf /usr/local/src/web_projop-aux-files.5.0.0.0.0.tgz
exit
{% endhighlight %}

### ProjectOpen Ayar Dosyasının Gözden Geçirilmesi

En azından `set servername` parametresinin uygun şekilde değiştirilmesi gerekmektedir.

{% highlight bash %}
nano /web/projop/etc/config.tcl
{% endhighlight %}

### PostgreSQL Veritabanının Oluşturulması

`projop` adından bir PostgreSQL veritabanı kullanıcısı oluşturun:

{% highlight bash %}
su - postgres
createuser -s projop
exit
{% endhighlight %}

Oluşturduğunuz bu kullanıcı için bi PostgreSQL veritabanını oluşturun:

{% highlight bash %}
su - projop
createdb --owner=projop --encoding=utf8 projop
createlang plpgsql projop
{% endhighlight %}

Demo veritabanını yükleyin:

{% highlight bash %}
psql -f ~/pg_dump.5.0.2.2.0.sql > import.log 2>&1
{% endhighlight %}

### Project Open'ın Çalıştığının Kontrolü

{% highlight bash %}
/usr/local/ns/bin/nsd -f -t /web/projop/etc/config.tcl -u projop -g projop
{% endhighlight %}

ProjectOpen, `[...] Notice: nssock: listening on 0.0.0.0:8000` yazmadan önce başlamaz. Komut satırındaki "Error" ve "Warning" satırlarını kontrol edin. ProjectOpen'a giriş yapmak için:

{% highlight bash %}
Sunucu Adresi: localhost:8000
Kullanıcı Adı: sysadmin@tigerpond.com
Şifre: system
{% endhighlight %}

Giriş yaptıktan sonra karşılaşacağınız ilk sayfa Yapılandırma Sihirbazı olacaktır. Lütfen, Yapılandırma Sihirbazıyla **henüz devam etmeyin**. çünkü ProjectOpen olması gerektiği gibi çalışmadan önce birkaç sorunun çözülmesi gerekiyor.

### NaviServer'ın Otomatik Başlatılması

NaviServer'ın otomatik başlatılması için bir systemd servisi oluşturun:

{% highlight bash %}
touch /lib/systemd/system/projop.service
nano /lib/systemd/system/projop.service
{% endhighlight %}

Oluşturulan dosya içerisine aşağıdaki satırları ekleyin:

{% highlight ini %}
[Unit]
Description=NaviServer Web Server as user projop
After=postgresql.service network.target
Wants=postgresql.service

[Service]
Type=forking
PIDFile=/web/projop/log/nsd.pid

ExecStartPre=/bin/rm -f /web/projop/log/nsd.pid
ExecStart=/usr/local/ns/bin/nsd -t /web/projop/etc/config.tcl -u projop -g projop
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s 9 $MAINPID

Restart=always
# Restart=on-abort
# Restart=on-abnormal

KillMode=process

[Install]
WantedBy=multi-user.target
{% endhighlight %}

systemd servisini etkinleştirin:

{% highlight bash %}
systemctl daemon-reload
systemctl enable projop.service
systemctl start projop.service
{% endhighlight %}

NaviServer'ın başlayıp başlamadığını kontrol edin:

{% highlight bash %}
tail -f /web/projop/log/error.log
{% endhighlight %}

### İşakış Çiziminin Düzeltilmesi

ProjectOpen'ın dinamik iş akışı özelliğinin çalışabilmesi için `graphviz` paketinin kurulmasına ve `/usr/local/bin/dot` dizininde `dot` ikiliğinin bulunmasına ihtiyaç duymaktadır. Ancak, Ubuntu `dot` ikiliğini varsayılan olarak `/usr/bin/dot` dizinine kurmaktadır. Bu yüzden beklenen konumda sembolik bir bağ oluşturun:

{% highlight bash %}
cd /usr/local/bin
ln -s /usr/bin/dot .
{% endhighlight %}

### Dosya Türleri Arasında Dönüşüm Sağlamak İçin LibreOffice'in Headless Olarak Çalıştırılması

`/lib/systemd/system/libreoffice-headless.service` dosyasını oluşturup aşağıdaki satırları içerisine ekleyin:

{% highlight ini %}
[Unit]
Description=Libre Office Headless Server
After=projop.service
Wants=httpd.service

[Service]
Type=simple
PIDFile=/run/libreoffice-server.pid
ExecStart=/usr/lib/libreoffice/program/soffice --headless --nologo --nofirststartwizard --accept="socket,host=localhost,port=8100;urp" &> /dev/null 2>&1
ExecStop=/bin/kill -s 9 $MAINPID

[Install]
WantedBy=multi-user.target
{% endhighlight %}

Systemd service dosyasını etkinleştirin:

{% highlight bash %}
systemctl daemon-reload
systemctl enable libreoffice-headless.service
{% endhighlight %}

### Otomatik Yedek Alma

ProjectOpen'ın sunduğu [betik](http://www.project-open.com/en/export-dbs) kullanılarak veritabanı otomatik olarak yedeklenebilir:

{% highlight perl %}
#!/usr/bin/perl

# --------------------------------------------------------------
# export-dbs
# Copyright 2009-2019 ]project-open[
# Frank Bergmann <frank.bergmann@project-open.com>
# Licensed under GPL V2.0 or higher
# --------------------------------------------------------------

# Constants, variables and parameters
#
my $debug =     1;

# email for error reports, you have to quote the "@"
my $email =     "if.gnu.linux@gmail.com";

# Plain-text password for encrypting backups stored in the cloud.
# Should not contain characters that need quoting in Perl/Bash.
#my $gpg_pass =  'secret';

# Where to store backups
#
my $exportdir = "/var/backup";
my $logdir =    "/var/log/backup";


# System commands
#
my $psql =      "/usr/bin/psql";
my $pg_dump =   "/usr/bin/pg_dump";
my $bzip2 =     "/usr/bin/bzip2";
my $gpg =       "/usr/bin/gpg2";

my $pg_owner =  "postgres";
my $computer_name = `hostname`;
my $time =      `/bin/date +\%Y\%m\%d.\%H\%M`;
my $weekday =   `/bin/date +%w`;

chomp($computer_name);
chomp($time);
chomp($weekday);

open(DBS, "su - $pg_owner -c '$psql -l' |");
while (my $db_line=<DBS>) {

        chomp($db_line);
        $db_line =~ /^\s*(\w*)/;
        my $db_name = $1;

        next if (length($db_name) < 2);
        next if ($db_name =~ /^\s$/);
        next if ($db_name =~ /^List$/);
        next if ($db_name =~ /^Name$/);

        next if ($db_name =~ /^postgres$/);
        next if ($db_name =~ /^template0$/);
        next if ($db_name =~ /^template1$/);

        my $file = "$exportdir/pgback.$computer_name.$db_name.$time.sql";
        my $log_file = "$logdir/export-dbs.$db_name.log";
        my $cmd = "su - $pg_owner --command='$pg_dump $db_name -c -O -F p -f $file' > $log_file 2>&1";
        print "export-dbs: $cmd\n" if ($debug);
        system $cmd;

        my $cmd2 = "su - $pg_owner --command='$bzip2 $file'";
        print "export-dbs: $cmd2\n" if ($debug);
        system $cmd2;

        #my $cmd3 = "echo $gpg_pass | $gpg --symmetric --batch --passphrase-fd 0 $file.bz2";
        #print "export-dbs: $cmd3\n" if ($debug);
        #system $cmd3;

        # Tar the entire web server to backup area, except for packages and filestorage backup.
        my $file9 = "$exportdir/webback.$computer_name.$db_name.$time.tgz";
        my $cmd9 = "tar --exclude='/web/$db_name/log' --exclude='/web/$db_name/filestorage/backup' -c -z -f $file9 /web/$db_name/";
        print "export-dbs: $cmd9\n" if ($debug);
        system $cmd9;

        # Analyze log file and send out error reports
        my $err_count = `cat $log_file | grep -i 'error:' | wc -l`;
        chomp($err_count);
        if ($err_count > 0) {
            system "cat $log_file | grep -i 'error:' | mail -s \"]project-open[ Backup Errors\" $email ";
        }

}
close(DBS);
{% endhighlight %}

Betiği root kullanıcının ev dizini altında oluşturun:

{% highlight bash %}
mkdir /root/bin
chmod +x /root/bin/export-dbs
{% endhighlight %}

Yedeklerin tutulacağı dizini oluşturun:

{% highlight bash %}
mkdir /var/backup
{% endhighlight %}

`projop` kullanıcısının ve PostgreSQL'in bu dizine yazmasına izin verin:

{% highlight bash %}
chown projop:postgres /var/backup
chmod g+w /var/backup
{% endhighlight %}

cron'a ilgili betiği ve eski yedeklerin silinmesini sağlayan komutları ekleyin:

{% highlight bash %}
29 3 * * * /usr/bin/perl /root/bin/export-dbs > /var/log/postgres/export-dbs.log 2>&1
24 3 * * * /usr/bin/find /export/backup -name '*.tgz' -mtime +6 | xargs rm
25 3 * * * /usr/bin/find /export/backup -name '*.bz2' -mtime +6 | xargs rm
{% endhighlight %}
