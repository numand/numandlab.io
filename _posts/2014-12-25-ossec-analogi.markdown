---
layout: post
title: "OSSEC"
subtitle: "AnaLogi Kurulumu ve Tanıtımı ve Sonsöz"
summary: "Analogi (Analytical Log Interface) Ossec‘in belli bir şema ile veri tabanına yazdığı bilgileri okuyarak, bu bilgilerin hangi sistem üzerinde (source), hangi kayıt dosyasında (path), hangi seviyede (level) ve hangi kural numarasında (rule id) ne kadar bulunduğunun zamana göre değişimini grafiksel olarak gösteren bir uygulamadır. "
part: "Bölüm 7"
date: 2014-12-25
categories: 
- linux
- hids
---

<span class="run-in">OSSEC'in tuttuğu kayıtları</span> okumak ve değerlendirmek için Ossec'in sunduğu kullanıcı arayüzüyle sınırlı değilsiniz. ANALOGI Ossec‘in belli bir şema ile veri tabanına yazdığı bilgileri okuyarak, bu bilgilerin hangi sistem üzerinde (source), hangi kayıt dosyasında (path), hangi seviyede (level) ve hangi kural numarasında (rule id) ne kadar bulunduğunun zamana göre değişimini grafiksel olarak gösteren bir uygulamadır.

## Kurulum

AnaLogi'nin çalışabilmesi için OSSEC'in veri tabanı desteğiyle derlenmiş olması gerek. Öncelikle derleme işlemi için gereken paketleri yükleyin.

{% highlight bash %}
sudo apt-get install build-essential make libssl-dev libmysqlclient-dev
{% endhighlight %}

MySQL yerine MariaDB kullanıyorsanız `libmysqlclient-dev` paketi yerine `libmariadbclient-dev` paketini kurun. Daha önceki bir OSSEC kurulumu üzerinden yeniden kurulum yapabilirsiniz. Hiçbir ayarınız kaybolmayacaktır, yalnız `/var/ossec/etc/client.keys` dosyasını yedekleyin. Kurulum bittikten sonra yerine kopyalayabilirsiniz.

{% highlight bash %}
cd /opt/ossec-hids-2.8.1/src/
sudo make setdb
Error: PostgreSQL client libraries not installed.
Info: Compiled with MySQL support.
{% endhighlight %}

*“Compiled with MySQL support”* ifadesi Ossec'e MySQL (MariaDB) desteğinin eklendiğini göstermektedir.

Ossec kurulumuna geçin:

{% highlight bash %}
cd ../
sudo ./install.sh
OSSEC HIDS v2.8 Kurulum Betiği - http://www.ossec.net
 OSSEC HIDS kurulum sürecini başlatmak üzeresiniz.
 Sisteminizde önceden kurulmuş bir C derleyicisi bulunmalıdır.
 Her türlü soru, öneri ve yorumlarınız için lütfen dcid@ossec.net
 (veya daniel.cid@gmail.com) adresine e-posta gönderiniz.
  - Sistem: Linux linux 3.16.0-24-generic
  - Kullanıcı: root
  - Bilgisayar: linux
  -- Devam etmek için ENTER veya çıkmak için Ctrl-C ye basın --
 - Bir OSSEC kurulumu mevcut. Güncellemek ister misiniz? (e/h): e
 Kuralları güncellemek ister misiniz? (e/h):
 - Yapılandırma doğru olarak tamamlandı.
 - OSSEC HIDS'i başlatmak için:
		/var/ossec/bin/ossec-control start
 - OSSEC HIDS'i durdurmak için:
		/var/ossec/bin/ossec-control stop
 - Yapılandırma buradan görülebilir veya değiştirilebilir: /var/ossec/etc/ossec.conf
    OSSEC HIDS kullandığınız için teşekkürler.
    Sorularınız, önerileriniz olursa veya her hangi bir yanlış
    bulursanız contact@ossec.net adresi ile veya kamuya açık
    e-posta listemiz ile ossec-list@ossec.net adresinden iletişime
    geçiniz.
    ( http://www.ossec.net/main/support/ ).
    http://www.ossec.net adresinde daha fazla bilgi bulunabilir.
    ---  Bitirmek için ENTER tuşuna basın (aşağıda daha fazla bilgi olabilir). ---
- Güncelleme tamamlandı.
{% endhighlight %}

Görüldüğü üzere kurulum betiği daha önceki kurulumu buldu ve sadece üzerine güncelleme yaptı. OSSEC'i yeniden başlattıktan sonra AnaLogi için bir veri tabanı ve kullanıcısı oluşturmanız gerek.

Veri tabanına giriş yapın.

{% highlight bash %}
mysql -u root -p
{% endhighlight %}

AnaLogi’nin kullanacağı veri tabanını oluşturun. Veri tabanı adına örnek olarak `ossec` seçilmiştir.

{% highlight bash %}
mysql> create database ossec;
{% endhighlight %}

Veri tabanı kullanıcısına gerekli izinleri atayın. Veri tabanı yöneticisi olarak `ossec_user` adı kullanılmıştır.

{% highlight bash %}
grant INSERT,SELECT,UPDATE,CREATE,DELETE,EXECUTE on ossec.* to ossec_user;
{% endhighlight %}

Veri tabanı yöneticisine şifre atayın.

{% highlight bash %}
set password for ossec_user = PASSWORD('birsey');
{% endhighlight %}

Çıkış yaptıktan sonra veri tabanının kullanacağı şemayı ekleyin.

{% highlight bash %}
mysql -u root -p ossec < /opt/ossec-hids-2.8.1/src/os_dbd/mysql.schema
{% endhighlight %}

OSSEC'in veri tabanını kullanabilmesi için `/var/ossec/etc/ossec.conf` dosyasına aşağıdaki satırları uygun değerleriyle doldurun.

{% highlight xml %}
<ossec_config>
    <database_output>
        <hostname>127.0.0.1</hostname>
        <username>ossec_user</username>
        <password>birsey</password>
        <database>ossec</database>
        <type>mysql</type>
    </database_output>
</ossec_config>
{% endhighlight %}

Eğer MySQL/MariaDB ayar dosyası `/etc/mysql/my.cnf` çerisinde `skip-networking` ve `bind-address = 127.0.0.1` değerleri bulunuyorsa bunları yorum satırına çevirip veri tabanını yeniden başlatın. Ardından, OSSEC'e veri tabanını kullanmasını söyleyip OSSEC'i yeniden başlatın.

{% highlight bash %}
sudo /var/ossec/bin/ossec-control enable database
sudo /var/ossec/bin/ossec-control restart
{% endhighlight %}

Bu işlemlerin ardından [AnaLogi] kurulumuna geçebilirsiniz. Kurulumu bir HTTP sunucunun çalıştığı dizinde yapacağız. Bu dizinin /var/www olduğu varsayılırsa, sırasıyla bu dizine girin; AnaLogi dosyalarını `git` komutuyla çekin; ayar dosyasının fazla uzantısından kurtulun.

{% highlight bash %}
cd /var/www/
sudo git clone https://github.com/ECSC/analogi.git
cd analogi
sudo cp db_ossec.php.new db_ossec.php
{% endhighlight %}

`db_ossec.php` dosyası içerisinde AnaLogi'nin veri tabanıyla konuşabilmesini sağlamak için gerekli değişiklikleri gerçekleştirin.

{% highlight php %}
define ('DB_USER_O', 'ossec_user');
define ('DB_PASSWORD_O', 'birsey');
define ('DB_HOST_O', '127.0.0.1');
define ('DB_NAME_O', 'ossec');
{% endhighlight %}

Son olarak `www-data` kullanıcısını `ossec` grubuna ekleyerek AnaLogi'nin */var/ossec* dizinini okuyabilmesini sağlayın.

{% highlight bash %}
sudo usermod -a -G ossec www-data
{% endhighlight %}

Bu adımdan sonra tek yapmanız gereken HTTP sunucunuzun */var/www/analogi* dizinini sunmasını sağlamaktır.

## Tanıtım

Analogi arayüzüne girdiğinizde sizi aşağıdaki gibi bir ekran karşılayacaktır.

![analogi_ana_sayfa][analogi_ana_sayfa]

Ön tanımlı olarak AnaLogi son 72 saat içinde gerçekleşen, seviyesi 7 ve yukarıda olan olayları kural numarasına göre göstermektedir. Bu ayarları `/var/www/analogi/config.php` dosyası üzerinden değiştirebilirsiniz. Seviyeyi değiştirmek için `$glb_level`; saati değiştirmek için `$glb_hours`; gruplandırmanın neye göre yapılacağını göstermek için `$glb_graphbreakdown` değişkenlerini değiştirebilirsiniz.

`AnaLogi Indeks` sayfasında kayıtlara ait bazı istatistikler ve bilgiler de görebilirsiniz.

![analogi_index_sayfasi][analogi_index_sayfasi]

`Haber Beslemesi (Newsfeed)` sekmesinde, kayıtlara ait eğilim analizlerini bulabilirsiniz.  Bu sekmeye ait değerler yine `/var/www/analogi/config.php` dosyası üzerinden değiştirilebilir.

![analogi_besleme_sayfasi][analogi_besleme_sayfasi]

`İzleme (mass monitoring)` sekmesi, olay kategorilerine göre olay sayısının zamana göre değişimini göstermektedir.

![analogi_gozlem_sayfasi][analogi_gozlem_sayfasi]

`Detay (detail)` sekmesinde, kayıt dosyalarında OSSEC'in bulduğu hata sayısının zamana göre değişimini gösteren bir grafik ile kayıt dosyalarındaki sorunların kural numarası, seviyesi, gerçekleştiği zaman, hangi kayıt dosyasında bulunduğu, hangi IP'den geldiği ve ne olduğu gösterilmektedir. Ayrıca sorunları filtreleyebilir ve sonuçları CVS olarak indirebilirsiniz.

![analogi_detay_sayfasi][analogi_detay_sayfasi]

![analogi_detay_sayfasi2][analogi_detay_sayfasi2]

`Yönetim (Management)` sekmesi bir kaç bölümden oluşmakta. `Giriş (Intro)` bölümünde bu sekmedeki diğer bölümlerin tanıtımı yapılmakta. `Kural ayarları (Rule tweaking)` bölümünde hangi kuralların veri tabanında en çok yeri kapladığı gösterilmekte. Buradaki kurallardan gereksiz olduğunu düşündüğünüzü kaldırabilir ya da ihtiyacınıza göre değiştirebilirsiniz.

![analogi_yonetim_sayfasi][analogi_yonetim_sayfasi]

Veri tabanı kullanımına ilişkin bölümler ise veri tabanı boyutunu, hangi sistemin hangi seviyedeki kurala ilişkin ne kadar veri gönderdiğini ve veri tabanındaki uyarı sayısının zamanla değişimini bir grafik aracılığıyla görebilmektesiniz.

![analogi_yonetim_sayfasi2][analogi_yonetim_sayfasi2]

Veri tabanı silme bölümünde ise belli bir tarihten daha eski olan ve belli bir aracıdan gelen uyarılar gibi çeşitli filtrelerle uyarıları veri tabanından kalıcı olarak silebilirsiniz.

![analogi_yonetim_sayfasi3][analogi_yonetim_sayfasi3]

## Sonsöz

Bütün bu adımları uygulamış ve OSSEC'i bir süre kullanmış biri olarak OSSEC'in kayıt dosyaları analizinde çok güzel bir iş çıkardığını söyleyebilirim. Ön tanımlı gelen çözücüler ve kurallar hemen hemen her ihtiyacınıza cevap verecek nitelikte. OSSEC'i AnaLogi ile beraber kullandığınızda sisteminizde neler olup bittiğini kayıt dosyaları arasında kaybolmadan görebilmektesiniz. Ayar dosyalarının XML dosya formatını kullanılması, bu dosya formatına âşina olmayanları ilk başlarda dosyaları okurken ve düzenlerken zorlayabilir fakat kısa sürede alışacaksınız. İhtiyaçlarınıza cevap verecek bir çözücü ve kural yazmak, OSSEC'in kullandığı özel düzenli ifadeler ve değişkenleri iyi bilmeyi gerektirmekte. OSSEC'in sunduğu yardım belgelerinin kimi yerde eksik kaldığını söyleyebilirim. Böyle durumlarda ihtiyacınızı karşılamak için başka kullanıcıların tecrübelerinden faydalanmak zorundasınız.

Yazının bana olduğu kadar size de faydalı olduğunu umarım. Bir sonraki yazıda görüşmek dileğiyle...

*[ANALOGI]: Analytical Log Interface
[AnaLogi]: https://github.com/ECSC/analogi
[analogi_ana_sayfa]: /assets/images/analogi_home.png
[analogi_index_sayfasi]: /assets/images/analogi_index.png
[analogi_besleme_sayfasi]: /assets/images/analogi_feed.png
[analogi_gozlem_sayfasi]: /assets/images/analogi_mass.png
[analogi_detay_sayfasi]: /assets/images/analogi_detay.png
[analogi_detay_sayfasi2]: /assets/images/analogi_detay2.png
[analogi_yonetim_sayfasi]: /assets/images/analogi_management.png
[analogi_yonetim_sayfasi2]: /assets/images/analogi_management2.png
[analogi_yonetim_sayfasi3]: /assets/images/analogi_management3.png