---
layout: post
title: "OSSEC"
subtitle: "Kullanıcı Arayüzü Kurulumu ve Tanıtımı"
summary: "Ossec'in ürettiği sonuçları komut satırında okuyabileceğiniz gibi bu işlemi kolaylaştıran bir kullanıcı arayüzü de bulunmaktadır. Kullanıcı arayüzünün geliştirilmesi 2.7.1 sürümünde son bulmasına rağmen yeni sürümle kullanılabilmekte.  "
part: "Bölüm 6"
date: 2014-12-18
categories: 
- linux
- hids
---

OSSEC'in ürettiği sonuçları komut satırında okuyabileceğiniz gibi bu işlemi kolaylaştıran bir kullanıcı arayüzü de bulunmaktadır. Kullanıcı arayüzünün geliştirilmesi 2.7.1 sürümünde son bulmasına rağmen yeni sürümle kullanılabilmekte. Kullanıcı arayüzü dosyasını OSSEC'i indirdiğiniz bağlantıdan temin edebilirsiniz.

Kurulumu yine /opt dizini altında gerçekleştireceğim. Paketi çekip md5 ve SHA1 bütünlüğünü denetleyin. Bütünlükler doğru ise paketi aynı dizine çıkarın.

{% highlight bash %}
cd /opt
sudo -U ossec wget http://www.ossec.net/files/ossec-wui-0.8.tar.gz
sudo md5sum ossec-wui-0.8.tar.gz
sudo sha1sum ossec-wui-0.8.tar.gz
sudo mkdir -p /var/www/ossec-web
sudo tar -xf ossec-wui-0.8.tar.gz -C /var/www/ossec-web --strip-components=1
{% endhighlight %}

Çıkardığınız dosyayı herhangi bir HTTP sunucunun çalıştığı bir dizine taşıyın. Apache HTTP sunucusu dışında başka bir sunucu (Nginx vs.) kullanıyorsanız apache2-utils paketini yüklemelisiniz. OSSEC kullanıcı arayüzü, bu paketle gelen htpasswd komutunu kullanılarak kurulum sırasında oluşturacağınız kullanıcıya parola atamaktadır.

{% highlight bash %}
sudo apt-get install apache2-utils
{% endhighlight %}

Artık kuruluma geçebilirsiniz.

{% highlight bash %}
cd /var/www/ossec-web
sudo ./setup.sh
{% endhighlight %}

Kurulum başlayınca kullanıcı arayüzünün bir HTTP sunucu ile çalıştırılabilmesi için bazı değerlerin girilmesi istenecek. Bu değerleri aşağıdaki gibi doldurun.

{% highlight bash %}
Setting up ossec ui...
 Username: www-data
 New password: 
 Re-type new password: 
 Adding password for user www-data
 Enter your web server user name (e.g. apache, www, nobody, www-data, ...)
 www-data
 Enter your OSSEC install directory path (e.g. /var/ossec)
 /var/ossec/
{% endhighlight %}

Son değeri girerken dizin sonunda **"/"** olmasına dikkat edin. Yoksa `/var/www/ossec-web/ossec_conf.php` dosyasındaki `$ossec_dir` değişkenine "/" olmadan yazılan dizinler OSSEC kullanıcı arayüzü tarafından görülmemektedir.

{% highlight bash %}
You must restart your web server after this setup is done.
Setup completed successfuly.
{% endhighlight %}

Cümlelerini gördüğünüzde OSSEC kullanıcı arayüzü kurulumu tamamlanmıştır fakat çalışması için gerekli bir kaç adım daha mevcut.

`www-data` kullanıcısının `ossec` grubunda olduğunu teyit edin:

{% highlight bash %}
grep ossec /etc/group
ossec:x:1001:www-data
{% endhighlight %}

Ossec arayüzünün `/var/ossec/tmp` dizinini okuyabilmesi için:

{% highlight bash %}
cd /var/ossec/
sudo chmod 770 tmp/
sudo chgrp www-data tmp/
{% endhighlight %}

komutlarını girmeniz gerekmekte. Bundan sonra OSSEC arayüzüne HTTP sunucusu ayar dosyasında belirttiğiniz adresten ulaşabilirsiniz. 

OSSEC kullanıcı arayüzünü açtığınızda sizi aşağıdaki gibi bir ekran karşılayacak. Ana (Main) sekmesinde OSSEC'in tuttuğu kayıtlara ait bilgiler gösterilmekte.

![ossec_gui_main][ossec_gui_main]

Arama (Search) sekmesinde, tutulan kayıtlar arasında filtreleme yaparak özellikle ilgilendiğiniz kayıtların gösterilmesini sağlayabilirsiniz. Buna dair örnek bir ekran görüntüsünü aşağıda görebilirsiniz.

![ossec_gui_search][ossec_gui_search]

Dosya bütünlüğü denetimi (Integrity checking) sekmesinde dosya bütünlüklerini ve en son hangi dosyaların değiştirildiğini görebilirsiniz.

![ossec_gui_integrity_check][ossec_gui_integrity_check]

İstatistikler (Stats) sekmesinde ise tutulan kayıtlara ait önemlilik seviyesine göre ortalama değer ve kural derecesine göre ortalama değer gibi istatistikleri görebilirsiniz.

![ossec_gui_stat][ossec_gui_stat]

[ossec_gui_main]: /assets/images/ossec_main.png
[ossec_gui_search]: /assets/images/ossec_search.png
[ossec_gui_integrity_check]: /assets/images/ossec_integrity.png
[ossec_gui_stat]: /assets/images/ossec_stat.png