---
layout: post
title: "Notes on The Leadership Gap"
subtitle: "Re-telling Lolly Daskal's Book"
summary: "Notes and summary of Lolloy Daskal's The Leadersip Gap "
date: 2019-09-08
language: "english"
categories:
- book
---

# Introduction

<span class="run-in">Lolly Daskal’s The Leadership Gap offers</span> the reader an inner journey in order to determine what kind of leader we are by questioning our past, thoughts, and behaviours. Although this book is positioned as a business book, it provides a framework based on human physiology and experiences. There are a lot of people who are not business leaders but can benefit from this book. This book’s lessons can be applied by teachers, community workers, parents, and anyone else who is in a position to decide.

> “Before advising someone, advise yourself. Why are you trying to straighten someone up while you have your own deficiency.” by Beydeba

To understand what makes Lolly a successful coach and develop her own coaching style, we need to understand what kind of person she is. From her biography, we learn that she questioned beyond what she was told and learned and sought answers from books, physiology, and observing human behaviour. Using the knowledge gained from different disciplines as a platform, she built her framework based on her experiences. There are three characteristics of her coaching style: meaningful philosophy, actionable methodology, and rationality.

Daskal ponders human behaviours and attitudes that make someone a leader and the shortcomings of a given leader type, which can drive someone to self-sabotage. It is often mentioned that people tend to overlook their own weaknesses, and to solve problems, they use a prior strategy that worked for them before. The problem is to think that the way you work is always the right way; to stop thinking about what if there is something that you need to learn; and to not adapt to time. The first step Lolly takes when she coaches is to make clients realise who they are. Whether you question yourself and your way is what makes someone a great leader, and how you apply your answers shows what kind of leader you are. In nine chapters, Daskal introduces us to seven leadership types, their shortcomings, which she describes as "gap", and their archetypes.

**The Rebel** is driven by *confidence*; and **the Imposter** is plagued by *self-doubt*.

**The Explorer** is fueled by *intuition*; **the Exploiter** is a master of *manipulation*.

**The Truth Teller** embraces *candour*; and her twin, **the Deceiver** creates *suspicion*.

**The Hero** embodies *courage*; and **the Bystander** is a *coward* if there ever was one.

**The Inventor** is brimming with *integrity*; and **the Destroyer** is morally *corrupt*.

**The Navigator** *trusts* and *is trusted*; and **the Fixer** is endlessly *arrogant*.

**The Knight**, whose *loyalty* is everything; and **the Mercenary**, who is perpetually *self-serving*.

The most important thing that makes someone a great leader is questioning themselves, being open to learning, changing, and growing. Sticking to what has always worked is the downfall of a leader.

# Concept Map

| Leadership Type | Characteristics | What | Gap | Archetype |
|:----------------|:----------------|:-----|:----|:----------|
| Rebel           | Confidence, Competence | Do everything in their strength to save the project, team, or company. | Self-doubt | Impostor |
| Explorer        | Intuition       | Who is not happy with the way things are and seeks new solutions and better ways | Manipulation | Exploiter |
| Truth Teller    | Candor          | Speak with candour and be ready to face consequences | Suspicion | Deceiver |
| Hero            | Courage         | Does not hesitate to act | Fear | Bystander |
| Inventor        | Integrity       | Improve processes or products to perfect their craft | Corrupt | Destroyer |
| Navigator       | Trusted         | Where they need to go, and they take people there. | Arrogant | Fixer |
| Knight          | Loyal           | Defend their beliefs and serve the organization and customer | Self-serving | Mercenary |

| Concept | Meaning |
|:--------|:--------|
| Confidence | Stems from skill, believing that you are able to do things you have to because of your skills. |
| Competence | It comes from knowledge, knowing that you have the knowledge to do things. |
| Self-doubt | Thinking why people listening to me although they are smarter, skillful and wiser than me |
| Intuition  | Knowledge gained from experiences |
| Manipulation | To use knowledge to overwhelm others and make others feel incompetent. To hide information from others to make them feel less competent |
| Candor | The force to create compelling companies, effective leadership, engaged employees, competitive advantages, and an ethical workplace. |
| Suspicion | A virus, once it infiltrates hearts and minds, affects how we think, act and lead |
| Courage | When we learn what we are afraid of, we can learn where courage is needed |
| Fear | See things and do nothing; hear things and do nothing |
| Frauds | They believe that they do not deserve success, and always feel guilty and ashamed about something. |
| Perfectionists | They believe that if something is not perfect, it is a failure. This belief weakens their confidence. |
| Operators | They do not feel good until all the things to be done are done perfectly. They shoulder all the work by themselves, which sets them up for failure. |
| Pleasers | They do everything to make people love them. They feel they need to add value to people. Otherwise, they feel badly about them. |
| Comparers | They think other people are smarter and better than themselves and put others before them in order to avoid notice. They live in their safe zone. This kind of mind makes them exhausted. |
| Saboteurs | They fear not only failure but also success. They do everything in their power to play small, not to fail and not to succeed. |
| Integrity | Identifying who you are, acknowledging your convictions to act accordingly, honouring your personal code of conduct, and how you will act. |
| Corruptness | Knowingly behave in a manner that is unethical |
| Trust | Trust begins from within. If you trust yourself, you trust your employees. |
| Arrogant | You do not admit that there may be something that you do not know. |
| Loyal | Doing a meaningful job, having trustworthy co-workers, a sense of belonging to  the organization, and a purpose. |
| Self-serving | Not interested in guiding others and the growth of others, lack of dedication |

# The Book

## Chapter One

To become a great leader, one needs to nurture both its own strong and weak qualities, expand talents, and develop deficiencies. One can become an executive by using one talent, but one needs many talents to become a leader. So one has to search for what it does not know in order to cultivate the skills that it needs. In this quest, one has to face the person one would rather not be, which is a result of fear, ignorance, shame, or rejection.

Since our mind is a very complex system, we can show a different kind of leadership based on time and circumstances, but we always tend to lean towards the same kind of leadership.

The leader in the first chapter, Michael, talked about his honour and not lying when asked about his success. Because he cheated on a test that would determine whether he could become a lawyer, his dream, his honour, and his pride were shattered. This incident drove him to make a hard decision: never lie again. Because of his high standard and not wanting everyone to know his lie from his high school years, he kept people away from him while pretending to close them. So he lied to himself. He could not be honest with him. One needs to accept that every human can make mistakes. The thing is not to allow our mistakes to weigh us down.

Lolly learned these truths from her experiences:

1. We are all capable of standing in our greatness: We allow negative things to stick to our thoughts and emotions, and because of this, we do not think that we can surpass ourselves.
2. We internalise our gaps: Before we learn which message we need to take and filter, we take all messages — negative, pessimistic, etc. — and these messages become part of us.
3. Negative messages create gaps: Once we take negative messages, they grow within us. Then they become obstacles between us and our dream.
4. Our secrets create large, deep gaps: Instead of dealing with our painful and embarrassing past, we tend to hide them. The more we hide them, the bigger they become.
5. When our gaps take control of us, we think we have lost: Our gaps trick us into thinking that we are incapable and unqualified. What we need to do is think that we can use our gaps to help us find our own way; they are our qualities.
6. You must own both sides of the gaps in your leadership: Everything is known by its opposite. Beauty is understood only because there is ugliness. Understanding our weaknesses is our greatest strength. Do not pretend you are something you are not.
7. Trust that you can stand in your greatness again: Because our gaps are what made us who we are, along with our good sides.

## Chapter Two

***The Rebels*** start revolutions, not revolts or uprisings. This type of leader does everything in their power to save a project, team, or company. Their strength comes from their ***confidence*** and ***competence***. Confidence is generally presented as the sole factor that makes one successful. We are taught that merely through sheer self-belief, we can achieve anything we want. However, we overlook the hardship and talent a successful person brings. *Confidence* stems from skill; believing that you are able to do things you have to because of your skills *Competence* comes from knowledge—knowing that you have the knowledge to do things.

***Irrational self-doubt***, thinking why people listen to me when they are smarter, more skilled, and wiser than me, is a syndrome that shows up when people do not feel truly competent. They think they achieved success by being lucky. Although they did everything they could to get where they are, they do not feel it was their competence that got them success. This chain of thoughts makes us the ***Impostor***. There are six types of Impostors:

*Frauds*: They believe that they do not deserve success, always feel guilty, and are ashamed about something. *Perfectionists*: They believe if something is not perfect, it is a failure. This belief weakens their confidence. *Operators*: They do not feel good until all the things to be done are done perfectly. They shoulder all the work by themselves, which sets them up for failure.
*Pleasers*: They do everything to make people love them. They feel they need to add value to people. Otherwise, they feel badly about them.
*Comparers*: They think other people are smarter and better than themselves and put others before them in order to avoid notice. They live in their safe zone. This kind of mind makes them exhausted.
*Saboteurs*: fear not only failure but also success. They do everything in their power to play small, not to fail, and not to succeed.

Overcoming impostor syndrome means breaking the chain of self-doubt. But changing is not an easy thing to do. We need to question our old thoughts. When we start to interrogate our thoughts, patterns, and beliefs, we will feel a great burden has been lifted from our shoulders. If you do things you know you can do, you can gain confidence. To overcome impostor syndrome:

1. Stop comparing yourself to others. There will always be people better than you. Everyone’s story is different. Instead of looking at someone, look at how far you've come and focus on self-improvement.
2. Remind yourself that there is no such thing as perfection. Remind yourself that perfection is not real. There are always failures in life. Aim for excellence and consider failures as learning opportunities.
3. Create an inner circle that supports you. We are taught that we have to be humble; otherwise, we will be perceived as arrogant. Create your own fab club that will cheer you on.

## Chapter Three

***The Explorers*** is someone who is not happy with the way things are and seeks new solutions and better ways. The thing that makes *the Explorers* successful is ***intuition***. They not only use their rational thought process but also their intuition to make a decision. *Intuition* is knowledge gained from experience. In order to cultivate intuition, one needs to accumulate wisdom by learning continually, obtain skills as much as possible, trust their inner sense, and take decisive action when needed. But when a leader does not allow their employees to think for themselves, when they micromanage or become controlling, they are leading with ***manipulation***. And they become the ***Exploiters***. *The Exploiters* used knowledge to overwhelm others and make them feel incompetent. They hide information from others to make them feel less competent. *The Exploiter* is unpredictable; it can be pleasant or wrathful, and it uses this to make others circle around.

To close the gap between *the Explorer* and *the Exploiter*, one needs to understand the difference between *intuition* and *manipulation*. Manipulation is always about making things better for yourself, whereas intuition is for others.

In order to not become an *Exploiter*, do not take advantage of people’s weaknesses; do not use people’s weaknesses against them; do not make others give up something to serve your own self-interest; and say what you mean.

## Chapter Four

The distinguished characteristic of ***the Truth Teller*** is ***candour***. *The Truth Tellers* speak with *candour* and are ready to face consequences. When you tell the truth, people follow your example and become more truthful with you. If you speak with candour, you create compelling companies, effective leadership, engaged employees, competitive advantages, and an ethical workplace. Honest leaders start a culture of candour within institutions. Otherwise, people are driven by mistrust and fear. When fear is present, lying begins. *The Truth Teller* is someone who communicates by sharing every piece of information to be told to employees. If information is held back, gossip and suspicion start to increase.

The gap in *the Truth Teller’s* leadership is ***suspicion***. *Suspicion* is a virus; once it infiltrates hearts and minds, it affects how we think, act, and lead. The archetype of *the Truth Teller* is ***the Deceiver***. *The Deceivers* can use their charm to make you believe their lies. They can manipulate people into taking a side or saying they trust them. *The Deceivers* can distract people from what really needs to be done or the subject of conservation, sometimes by using body language. They do not take responsibility and blame someone else. You cannot trust *the Deceivers*. When the circumstances change, they will abandon you. To confront *the Deceiver*:

1. If we have something about which we feel inadequate or vulnerable, there is no need to hide it with lies. We have to accept who we really are, change our direction, and make things different.
2. If we try to hide the truth ourselves, we inhibit our ability to make wise and helpful decisions.
3. We are not perfect beings. We have to admit that we do not know everything. Power is not in lying; it is in truth.

## Chapter Five

The distinguished characteristic of ***The Hero*** is ***courage***. Although circumstances are against it, *the Hero* does not hesitate to act. When we learn what we are afraid of, we can learn where courage is needed. If we lighten our fear, we strengthen our courage, and courage strengthens us. The word "hero" is related to the Latin word "servo", which means to serve. A leader is not someone who just leads but also serves its employees, clients, and communities. If we lead with only hierarchy, it will be short-lived. But if we lead as heroes with a servant heart, we can accomplish any goals we set for ourselves and our organisations. We need to prepare a place where everyone can be themselves and courageous in order to make people not afraid of saying what is wrong. When a courageous culture has planted within a firm, people seek leadership opportunities, try new thing out of their skills, offering ideas.

The gap in *the Hero's* leadership is ***fear***. The archetype of *the Hero* is ***the Bystander***.

In order to not become a bystander:

1. If you see something—bad behaviour, disrespect, misconduct—that needs to be addressed, act immediately to keep a situation from escalating.
2. If you want to be successful, you have to be brave now, not next time. You cannot sit idly and wish something to happen.

## Chapter Six

What makes a sushi master is becoming a specialist, attaining the best ingredients via good relationships, and always seeking better quality standards.

***The Inventor*** can be described as a person who seeks to always improve what is available to him by taking small risks in order to achieve big success. *The Inventors* have high standards and ask everyone around them to abide by them so that a faithful team can be built to accomplish their vision. *Inventors* can be successful if they have ***integrity***. *Integrity* means knowing who you are, recognising your beliefs, being yourself, and abiding by your etiquette. By attaining integrity, you become a whole person. And if you become a whole, you are ready to shoulder the issues of your innovations. Integrity is the result of being respectful of what you do, being honest with yourself and everyone, having a coherent moral code, having a firm conviction, and building trust within your team.

But when you knowingly behave in a manner that is unethical, what you await is ***corruption***. There are several enticing things that can deflect us from the right path or negative emotions that can lead to corruption: arrogance without humility, anger without consciousness, and bias without indulgence. The mind can easily give in, but the heart fights for your moral code, honesty, and conviction. If you give in, these negative emotions will open a gap from which ***The destroyer*** will find a way to surface. And do not forget that corruption needs an alibi to feel right.

Instead of integrating people with a code of conduct into a common goal, they destroy not only their businesses but also their minds and hearts. *The Destroyer’s* lack of commitment causes jobs to be done sloppily. In order not to become *The Destroyer*, you have to check yourself by asking whether your idea is serving the good of many or just yourself. *The Destroyer* tends to look for what is bad, whereas *the Inventor* looks positively. *The Destroyer* always finds a fault and criticises. Instead of looking for what is wrong, try to find what is right and let them surpass themselves. If you want to be a person to be looked upon, do what you say: be trustworthy, respectful, and loyal.

## Chapter Seven

**The Navigator*** is pragmatic, decisive (telling people what to do), knowing (I know everything), and ***trusted*** (trust my decisions). This type of leadership can lead to becoming ***the Fixer***: impetuous, ***arrogant***, and egoistical because they do not admit that there may be something that they do not know. If someone wants to fill the gap stemming from being a *Navigator*, the first step that needs to be taken is to listen to others and ask questions about themselves.

The key to success for *The Navigator* is to trust yourself and to build trust in others. *Trust* begins from within. If you trust yourself, you trust your employees. Because when you trust someone, you know that you will not be hurt by them. Without facade and worry, you build a relationship and work together, and by doing this, you achieve much more than if you worked alone. But if you just expect others to follow you by becoming a demanding person, you lead no one. Do not think that you know everything or what works best for others. This kind of thinking turns you into *The Fixer*. Because you think you are a know-it-all and know what works best for others, you force yourself upon others to fix them. Then you start to feel needed and want other people to need you. And you become an emotionally dependent person, which means you are no longer a navigator anymore. If you cannot withhold yourself from helping others the way you want and are not satisfied with how others do their job, you are a micromanager. Micromanagers always exhaust themselves. To get out of micromanagement, give yourself enough time to think about trust and how it can help your leadership.

*The Navigator’s* role and trust can be best understood by looking at the relationship between a conductor and his orchestra. A conductor must trust his orchestra that they will deliver the music that he wants, and an orchestra must trust its conductor’s every move. Only through mutual trust can beautiful music be obtained. A leader can show trust in others by honouring them, admiring their skills, and appreciating a job well done.

## Chapter Eight

***The Knights*** stand beside you to serve you. They believe in their mission, organisation, and customers. They are ***loyal*** to their beliefs, organisation, and customers. With loyalty, an organisation acts as a unit, builds a firm relationship, and protects its employees. By standing next to the Knight, you feel safe, and you can take the initiative to do something in order to create new opportunities and successes. However, there are some conditions that need to be addressed to become *loyal*: doing a meaningful job, having trustworthy co-workers, a sense of belonging to the organisation, and having a purpose.

Opposite of *the Knight* is ***The Mercenary***: the ***self-serving*** leaders. They are not interested in guiding their workers and growth of others. They lack the dedication to lead others. They do not safeguard their workers. If a leader does not have its employees back, it can not expect loyalty from them. *The Mercenary* does not feel responsible for mistakes and blame others, which lead to distrust within organization.

Opposite *the Knight* is ***The mercenary***, the ***self-serving*** leader. They are not interested in guiding their workers or the growth of others. They lack the dedication to lead others. They do not safeguard their workers. If a leader does not have its employees backs, it cannot expect loyalty from them. *The Mercenary* does not feel responsible for mistakes and blames others, which leads to distrust within the organisation.
