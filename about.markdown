---
layout: about
---

## Hakkımda

2009'dan beri Ubuntu başta olmak üzere GNU/Linux işletim sistemlerini kullanmaktayım. Bilgim dâhilinde yerel Ubuntu forumunda kullanıcılara yardım etmenin yanı sıra, çeşitli özgür ve açık kaynak yazılımların çevirilerine ve hata ayıklamasına yardımcı olmaktayım.

## İletişim

Bana her zaman [e-posta] adresimi kullanarak ulaşabilirsiniz. GPG ile şifrelenmiş bir e-posta göndermek isterseniz <a href="/assets/numandemirdogen.asc">PGP anahtarımı</a> kullanabilirsiniz.


## Site Hakkında

Bu sitenin içeriği (yazılar, çizimler vs.) aksi belirtilmedikçe [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)] lisansı ile lisanslanmıştır.

### Tasarım

Sitenin tasarımı bu [tema] temel alınarak oluşturulmuştur. Site genelinde [Solarized] renk paletindeki renkler kullanılmaktadır. Her ne kadar CSS'lerde herhangi bir font belirtilmese de tasarım [Libertinus] font ailesi kullanılarak yapılmıştır. CSS ve HTML gibi site kaynak kodları [GNU GPLv3] lisansı ile lisanslanmıştır.

### Teknoloji

Bu site [Gitlab Pages] alt yapısını kullanarak [Jekyll] statik site oluşturucusu vasıtasıyla sunulmaktadır.

[e-posta]: mailto:if.gnu.linux@gmail.com
[Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)]: https://creativecommons.org/licenses/by-sa/4.0/
[tema]: https://github.com/ruuda/blog
[Solarized]: http://ethanschoonover.com/solarized
[Libertinus]: https://github.com/alerque/libertinus
[GNU GPLv3]: https://www.gnu.org/licenses/gpl-3.0.html
[Gitlab Pages]: https://pages.gitlab.io/
[Jekyll]: https://jekyllrb.com/
