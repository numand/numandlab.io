[![build status](https://gitlab.com/numand/numand.gitlab.io/badges/master/build.svg)](https://gitlab.com/numand/numand.gitlab.io/commits/master)

---
**TR:**

Gitlab Pages üzerindeki [Jekyll] kullanılarak oluşturulan **[sitem]**.

Site için GPLv3+ lisanslı Beste teması kullanılmaktadır.

---
**EN:**

Personal **[blog]** using Jekyll on Gitlab Pages

----

GitLab Pages hakkında daha fazla bilgi için: https://pages.gitlab.io

Belgeler: http://doc.gitlab.com/ee/pages/README.html.

---

[Jekyll]: https://jekyllrb.com/
[sitem]: https://numand.gitlab.io
[blog]: https://numand.gitlab.io
